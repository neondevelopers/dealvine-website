-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 12, 2016 at 10:09 AM
-- Server version: 5.7.14
-- PHP Version: 7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `octobercms_angularjs`
--

-- --------------------------------------------------------

--
-- Table structure for table `backend_access_log`
--

CREATE TABLE `backend_access_log` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `ip_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `backend_access_log`
--

INSERT INTO `backend_access_log` (`id`, `user_id`, `ip_address`, `created_at`, `updated_at`) VALUES
(1, 1, '::1', '2016-11-28 01:19:21', '2016-11-28 01:19:21'),
(2, 1, '::1', '2016-11-29 03:20:45', '2016-11-29 03:20:45');

-- --------------------------------------------------------

--
-- Table structure for table `backend_users`
--

CREATE TABLE `backend_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `login` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `activation_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `persist_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reset_password_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `is_activated` tinyint(1) NOT NULL DEFAULT '0',
  `activated_at` timestamp NULL DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `backend_users`
--

INSERT INTO `backend_users` (`id`, `first_name`, `last_name`, `login`, `email`, `password`, `activation_code`, `persist_code`, `reset_password_code`, `permissions`, `is_activated`, `activated_at`, `last_login`, `created_at`, `updated_at`, `is_superuser`) VALUES
(1, 'Admin', 'Person', 'admin', 'mtl.golakiya@gmail.com', '$2y$10$uEHe3QTeyfjf28HQamFRBOWdIv4mZHbP03xDUYmhOxP7d58b/Y9ky', NULL, '$2y$10$Y5dk.5rxfzs3t1X7Bh2n8.ZzcpG5J7tLhxVQkWGZKJhsXks9Vi86y', NULL, '', 1, NULL, '2016-11-29 03:20:45', '2016-11-28 00:29:35', '2016-11-29 03:20:45', 1);

-- --------------------------------------------------------

--
-- Table structure for table `backend_users_groups`
--

CREATE TABLE `backend_users_groups` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_group_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `backend_users_groups`
--

INSERT INTO `backend_users_groups` (`user_id`, `user_group_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `backend_user_groups`
--

CREATE TABLE `backend_user_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `is_new_user_default` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `backend_user_groups`
--

INSERT INTO `backend_user_groups` (`id`, `name`, `permissions`, `created_at`, `updated_at`, `code`, `description`, `is_new_user_default`) VALUES
(1, 'Owners', NULL, '2016-11-28 00:29:35', '2016-11-28 00:29:35', 'owners', 'Default group for website owners.', 0);

-- --------------------------------------------------------

--
-- Table structure for table `backend_user_preferences`
--

CREATE TABLE `backend_user_preferences` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `namespace` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `group` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `item` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `backend_user_throttle`
--

CREATE TABLE `backend_user_throttle` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attempts` int(11) NOT NULL DEFAULT '0',
  `last_attempt_at` timestamp NULL DEFAULT NULL,
  `is_suspended` tinyint(1) NOT NULL DEFAULT '0',
  `suspended_at` timestamp NULL DEFAULT NULL,
  `is_banned` tinyint(1) NOT NULL DEFAULT '0',
  `banned_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `backend_user_throttle`
--

INSERT INTO `backend_user_throttle` (`id`, `user_id`, `ip_address`, `attempts`, `last_attempt_at`, `is_suspended`, `suspended_at`, `is_banned`, `banned_at`) VALUES
(1, 1, '::1', 0, NULL, 0, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cache`
--

CREATE TABLE `cache` (
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` longtext COLLATE utf8_unicode_ci NOT NULL,
  `expiration` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_theme_data`
--

CREATE TABLE `cms_theme_data` (
  `id` int(10) UNSIGNED NOT NULL,
  `theme` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` mediumtext COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `deferred_bindings`
--

CREATE TABLE `deferred_bindings` (
  `id` int(10) UNSIGNED NOT NULL,
  `master_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `master_field` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slave_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slave_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `session_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_bind` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` int(10) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8_unicode_ci NOT NULL,
  `queue` text COLLATE utf8_unicode_ci NOT NULL,
  `payload` text COLLATE utf8_unicode_ci NOT NULL,
  `failed_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payload` text COLLATE utf8_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2013_10_01_000001_Db_Deferred_Bindings', 1),
('2013_10_01_000002_Db_System_Files', 1),
('2013_10_01_000003_Db_System_Plugin_Versions', 1),
('2013_10_01_000004_Db_System_Plugin_History', 1),
('2013_10_01_000005_Db_System_Settings', 1),
('2013_10_01_000006_Db_System_Parameters', 1),
('2013_10_01_000007_Db_System_Add_Disabled_Flag', 1),
('2013_10_01_000008_Db_System_Mail_Templates', 1),
('2013_10_01_000009_Db_System_Mail_Layouts', 1),
('2014_10_01_000010_Db_Jobs', 1),
('2014_10_01_000011_Db_System_Event_Logs', 1),
('2014_10_01_000012_Db_System_Request_Logs', 1),
('2014_10_01_000013_Db_System_Sessions', 1),
('2015_10_01_000014_Db_System_Mail_Layout_Rename', 1),
('2015_10_01_000015_Db_System_Add_Frozen_Flag', 1),
('2015_10_01_000016_Db_Cache', 1),
('2015_10_01_000017_Db_System_Revisions', 1),
('2015_10_01_000018_Db_FailedJobs', 1),
('2016_10_01_000019_Db_System_Plugin_History_Detail_Text', 1),
('2016_10_01_000020_Db_System_Timestamp_Fix', 1),
('2013_10_01_000001_Db_Backend_Users', 2),
('2013_10_01_000002_Db_Backend_User_Groups', 2),
('2013_10_01_000003_Db_Backend_Users_Groups', 2),
('2013_10_01_000004_Db_Backend_User_Throttle', 2),
('2014_01_04_000005_Db_Backend_User_Preferences', 2),
('2014_10_01_000006_Db_Backend_Access_Log', 2),
('2014_10_01_000007_Db_Backend_Add_Description_Field', 2),
('2015_10_01_000008_Db_Backend_Add_Superuser_Flag', 2),
('2016_10_01_000009_Db_Backend_Timestamp_Fix', 2),
('2014_10_01_000001_Db_Cms_Theme_Data', 3),
('2016_10_01_000002_Db_Cms_Timestamp_Fix', 3);

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payload` text COLLATE utf8_unicode_ci,
  `last_activity` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_event_logs`
--

CREATE TABLE `system_event_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `level` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8_unicode_ci,
  `details` mediumtext COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `system_event_logs`
--

INSERT INTO `system_event_logs` (`id`, `level`, `message`, `details`, `created_at`, `updated_at`) VALUES
(1, 'error', 'Cms\\Classes\\CmsException: The content file \'welcome.htm\' is not found. in /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/modules/cms/Classes/Controller.php:951\nStack trace:\n#0 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/modules/cms/Twig/Extension.php(128): Cms\\Classes\\Controller->renderContent(\'welcome.htm\', Array)\n#1 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/storage/cms/twig/98/9862e1fca93b8cbc6a4d00688061c2bfd8762ec71623f5eb313e94d76b51cb26.php(24): Cms\\Twig\\Extension->contentFunction(\'welcome.htm\', Array)\n#2 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/twig/twig/lib/Twig/Template.php(428): __TwigTemplate_8168a4a0516415b48969fb8a3120e833a568ea3198a1e564f2516218e2557131->doDisplay(Array, Array)\n#3 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/twig/twig/lib/Twig/Template.php(396): Twig_Template->displayWithErrorHandling(Array, Array)\n#4 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/twig/twig/lib/Twig/Template.php(407): Twig_Template->display(Array)\n#5 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/modules/cms/Classes/Controller.php(378): Twig_Template->render(Array)\n#6 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/modules/cms/Classes/Controller.php(216): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#7 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/modules/cms/Classes/CmsController.php(48): Cms\\Classes\\Controller->run(\'/\')\n#8 [internal function]: Cms\\Classes\\CmsController->run(\'/\')\n#9 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(256): call_user_func_array(Array, Array)\n#10 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(164): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#11 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(112): Illuminate\\Routing\\ControllerDispatcher->call(Object(Cms\\Classes\\CmsController), Object(Illuminate\\Routing\\Route), \'run\')\n#12 [internal function]: Illuminate\\Routing\\ControllerDispatcher->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#13 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(139): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#14 [internal function]: Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#15 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#16 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(114): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#17 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(67): Illuminate\\Routing\\ControllerDispatcher->callWithinStack(Object(Cms\\Classes\\CmsController), Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request), \'run\')\n#18 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Routing/Route.php(203): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request), \'Cms\\\\Classes\\\\Cms...\', \'run\')\n#19 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Routing/Route.php(134): Illuminate\\Routing\\Route->runWithCustomDispatcher(Object(Illuminate\\Http\\Request))\n#20 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Routing/Router.php(708): Illuminate\\Routing\\Route->run(Object(Illuminate\\Http\\Request))\n#21 [internal function]: Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#22 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(139): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#23 [internal function]: Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#24 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#25 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Routing/Router.php(710): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#26 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Routing/Router.php(673): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#27 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#28 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(236): Illuminate\\Routing\\Router->dispatch(Object(Illuminate\\Http\\Request))\n#29 [internal function]: Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#30 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(139): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#31 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#32 [internal function]: Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#33 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(124): call_user_func_array(Array, Array)\n#34 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(62): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#35 [internal function]: Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#36 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(124): call_user_func_array(Array, Array)\n#37 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#38 [internal function]: Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#39 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(124): call_user_func_array(Array, Array)\n#40 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(59): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#41 [internal function]: Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#42 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(124): call_user_func_array(Array, Array)\n#43 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(44): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#44 [internal function]: Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#45 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(124): call_user_func_array(Array, Array)\n#46 [internal function]: Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#47 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#48 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(122): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#49 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(87): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#50 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#51 {main}\n\nNext Twig_Error_Runtime: An exception has been thrown during the rendering of a template ("The content file \'welcome.htm\' is not found.") in "/Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/themes/dealvine/pages/home.htm" at line 3. in /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/twig/twig/lib/Twig/Template.php:443\nStack trace:\n#0 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/twig/twig/lib/Twig/Template.php(396): Twig_Template->displayWithErrorHandling(Array, Array)\n#1 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/twig/twig/lib/Twig/Template.php(407): Twig_Template->display(Array)\n#2 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/modules/cms/Classes/Controller.php(378): Twig_Template->render(Array)\n#3 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/modules/cms/Classes/Controller.php(216): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#4 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/modules/cms/Classes/CmsController.php(48): Cms\\Classes\\Controller->run(\'/\')\n#5 [internal function]: Cms\\Classes\\CmsController->run(\'/\')\n#6 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(256): call_user_func_array(Array, Array)\n#7 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(164): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#8 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(112): Illuminate\\Routing\\ControllerDispatcher->call(Object(Cms\\Classes\\CmsController), Object(Illuminate\\Routing\\Route), \'run\')\n#9 [internal function]: Illuminate\\Routing\\ControllerDispatcher->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#10 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(139): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#11 [internal function]: Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#12 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#13 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(114): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#14 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(67): Illuminate\\Routing\\ControllerDispatcher->callWithinStack(Object(Cms\\Classes\\CmsController), Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request), \'run\')\n#15 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Routing/Route.php(203): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request), \'Cms\\\\Classes\\\\Cms...\', \'run\')\n#16 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Routing/Route.php(134): Illuminate\\Routing\\Route->runWithCustomDispatcher(Object(Illuminate\\Http\\Request))\n#17 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Routing/Router.php(708): Illuminate\\Routing\\Route->run(Object(Illuminate\\Http\\Request))\n#18 [internal function]: Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#19 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(139): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#20 [internal function]: Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#21 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#22 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Routing/Router.php(710): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#23 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Routing/Router.php(673): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#24 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#25 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(236): Illuminate\\Routing\\Router->dispatch(Object(Illuminate\\Http\\Request))\n#26 [internal function]: Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#27 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(139): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#28 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#29 [internal function]: Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#30 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(124): call_user_func_array(Array, Array)\n#31 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(62): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#32 [internal function]: Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#33 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(124): call_user_func_array(Array, Array)\n#34 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#35 [internal function]: Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#36 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(124): call_user_func_array(Array, Array)\n#37 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(59): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#38 [internal function]: Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#39 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(124): call_user_func_array(Array, Array)\n#40 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(44): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#41 [internal function]: Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#42 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(124): call_user_func_array(Array, Array)\n#43 [internal function]: Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#44 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#45 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(122): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#46 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(87): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#47 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#48 {main}', NULL, '2016-11-28 01:19:43', '2016-11-28 01:19:43'),
(2, 'error', 'Twig_Error_Syntax: Unexpected character "$" in "/Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/themes/dealvine/pages/home.htm" at line 81. in /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/twig/twig/lib/Twig/Lexer.php:292\nStack trace:\n#0 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/twig/twig/lib/Twig/Lexer.php(224): Twig_Lexer->lexExpression()\n#1 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/twig/twig/lib/Twig/Lexer.php(123): Twig_Lexer->lexVar()\n#2 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/twig/twig/lib/Twig/Environment.php(645): Twig_Lexer->tokenize(Object(Twig_Source))\n#3 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/twig/twig/lib/Twig/Environment.php(742): Twig_Environment->tokenize(Object(Twig_Source))\n#4 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/twig/twig/lib/Twig/Environment.php(443): Twig_Environment->compileSource(Object(Twig_Source))\n#5 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/modules/cms/Classes/Controller.php(377): Twig_Environment->loadTemplate(\'/Users/Mitul/Do...\')\n#6 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/modules/cms/Classes/Controller.php(216): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#7 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/modules/cms/Classes/CmsController.php(48): Cms\\Classes\\Controller->run(\'/\')\n#8 [internal function]: Cms\\Classes\\CmsController->run(\'/\')\n#9 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(256): call_user_func_array(Array, Array)\n#10 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(164): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#11 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(112): Illuminate\\Routing\\ControllerDispatcher->call(Object(Cms\\Classes\\CmsController), Object(Illuminate\\Routing\\Route), \'run\')\n#12 [internal function]: Illuminate\\Routing\\ControllerDispatcher->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#13 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(139): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#14 [internal function]: Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#15 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#16 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(114): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#17 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(67): Illuminate\\Routing\\ControllerDispatcher->callWithinStack(Object(Cms\\Classes\\CmsController), Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request), \'run\')\n#18 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Routing/Route.php(203): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request), \'Cms\\\\Classes\\\\Cms...\', \'run\')\n#19 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Routing/Route.php(134): Illuminate\\Routing\\Route->runWithCustomDispatcher(Object(Illuminate\\Http\\Request))\n#20 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Routing/Router.php(708): Illuminate\\Routing\\Route->run(Object(Illuminate\\Http\\Request))\n#21 [internal function]: Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#22 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(139): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#23 [internal function]: Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#24 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#25 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Routing/Router.php(710): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#26 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Routing/Router.php(673): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#27 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#28 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(236): Illuminate\\Routing\\Router->dispatch(Object(Illuminate\\Http\\Request))\n#29 [internal function]: Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#30 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(139): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#31 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#32 [internal function]: Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#33 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(124): call_user_func_array(Array, Array)\n#34 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(62): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#35 [internal function]: Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#36 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(124): call_user_func_array(Array, Array)\n#37 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#38 [internal function]: Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#39 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(124): call_user_func_array(Array, Array)\n#40 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(59): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#41 [internal function]: Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#42 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(124): call_user_func_array(Array, Array)\n#43 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(44): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#44 [internal function]: Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#45 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(124): call_user_func_array(Array, Array)\n#46 [internal function]: Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#47 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#48 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(122): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#49 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(87): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#50 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine/october-angularjs/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#51 {main}', NULL, '2016-11-28 05:27:48', '2016-11-28 05:27:48'),
(3, 'error', 'Twig_Error_Syntax: Unexpected token "end of print statement" of value "" in "/Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine-website/themes/dealvine/layouts/default.htm" at line 38. in /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine-website/vendor/twig/twig/lib/Twig/ExpressionParser.php:208\nStack trace:\n#0 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine-website/vendor/twig/twig/lib/Twig/ExpressionParser.php(102): Twig_ExpressionParser->parsePrimaryExpression()\n#1 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine-website/vendor/twig/twig/lib/Twig/ExpressionParser.php(55): Twig_ExpressionParser->getPrimary()\n#2 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine-website/vendor/twig/twig/lib/Twig/Parser.php(150): Twig_ExpressionParser->parseExpression()\n#3 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine-website/vendor/twig/twig/lib/Twig/Parser.php(106): Twig_Parser->subparse(NULL, false)\n#4 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine-website/vendor/twig/twig/lib/Twig/Environment.php(684): Twig_Parser->parse(Object(Twig_TokenStream))\n#5 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine-website/vendor/twig/twig/lib/Twig/Environment.php(742): Twig_Environment->parse(Object(Twig_TokenStream))\n#6 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine-website/vendor/twig/twig/lib/Twig/Environment.php(443): Twig_Environment->compileSource(Object(Twig_Source))\n#7 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine-website/modules/cms/Classes/Controller.php(387): Twig_Environment->loadTemplate(\'/Users/Mitul/Do...\')\n#8 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine-website/modules/cms/Classes/Controller.php(216): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#9 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine-website/modules/cms/Classes/CmsController.php(48): Cms\\Classes\\Controller->run(\'/\')\n#10 [internal function]: Cms\\Classes\\CmsController->run(\'/\')\n#11 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine-website/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(256): call_user_func_array(Array, Array)\n#12 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine-website/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(164): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#13 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine-website/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(112): Illuminate\\Routing\\ControllerDispatcher->call(Object(Cms\\Classes\\CmsController), Object(Illuminate\\Routing\\Route), \'run\')\n#14 [internal function]: Illuminate\\Routing\\ControllerDispatcher->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#15 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine-website/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(139): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#16 [internal function]: Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#17 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine-website/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#18 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine-website/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(114): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#19 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine-website/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(67): Illuminate\\Routing\\ControllerDispatcher->callWithinStack(Object(Cms\\Classes\\CmsController), Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request), \'run\')\n#20 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine-website/vendor/laravel/framework/src/Illuminate/Routing/Route.php(203): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request), \'Cms\\\\Classes\\\\Cms...\', \'run\')\n#21 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine-website/vendor/laravel/framework/src/Illuminate/Routing/Route.php(134): Illuminate\\Routing\\Route->runWithCustomDispatcher(Object(Illuminate\\Http\\Request))\n#22 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine-website/vendor/laravel/framework/src/Illuminate/Routing/Router.php(708): Illuminate\\Routing\\Route->run(Object(Illuminate\\Http\\Request))\n#23 [internal function]: Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#24 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine-website/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(139): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#25 [internal function]: Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#26 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine-website/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#27 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine-website/vendor/laravel/framework/src/Illuminate/Routing/Router.php(710): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#28 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine-website/vendor/laravel/framework/src/Illuminate/Routing/Router.php(673): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#29 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine-website/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#30 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine-website/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(236): Illuminate\\Routing\\Router->dispatch(Object(Illuminate\\Http\\Request))\n#31 [internal function]: Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#32 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine-website/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(139): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#33 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine-website/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#34 [internal function]: Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#35 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine-website/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(124): call_user_func_array(Array, Array)\n#36 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine-website/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(62): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#37 [internal function]: Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#38 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine-website/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(124): call_user_func_array(Array, Array)\n#39 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine-website/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#40 [internal function]: Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#41 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine-website/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(124): call_user_func_array(Array, Array)\n#42 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine-website/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(59): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#43 [internal function]: Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#44 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine-website/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(124): call_user_func_array(Array, Array)\n#45 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine-website/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(44): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#46 [internal function]: Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#47 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine-website/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(124): call_user_func_array(Array, Array)\n#48 [internal function]: Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#49 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine-website/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#50 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine-website/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(122): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#51 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine-website/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(87): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#52 /Users/Mitul/Documents/Work/www/MyFreelancing/NeonMobile/dealvine-website/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#53 {main}', NULL, '2016-11-29 03:20:36', '2016-11-29 03:20:36');
INSERT INTO `system_event_logs` (`id`, `level`, `message`, `details`, `created_at`, `updated_at`) VALUES
(4, 'error', 'PDOException: SQLSTATE[42S02]: Base table or view not found: 1146 Table \'octobercms_angularjs.raviraj_rjsliders_sliders\' doesn\'t exist in C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php:319\nStack trace:\n#0 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(319): PDO->prepare(\'select * from `...\')\n#1 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(655): Illuminate\\Database\\Connection->Illuminate\\Database\\{closure}(Object(Illuminate\\Database\\MySqlConnection), \'select * from `...\', Array)\n#2 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(618): Illuminate\\Database\\Connection->runQueryCallback(\'select * from `...\', Array, Object(Closure))\n#3 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(324): Illuminate\\Database\\Connection->run(\'select * from `...\', Array, Object(Closure))\n#4 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Query\\Builder.php(1431): Illuminate\\Database\\Connection->select(\'select * from `...\', Array, true)\n#5 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Query\\Builder.php(1408): Illuminate\\Database\\Query\\Builder->runSelect()\n#6 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\october\\rain\\src\\Database\\QueryBuilder.php(76): Illuminate\\Database\\Query\\Builder->get(Array)\n#7 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Eloquent\\Builder.php(477): October\\Rain\\Database\\QueryBuilder->get(Array)\n#8 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Eloquent\\Builder.php(234): Illuminate\\Database\\Eloquent\\Builder->getModels(Array)\n#9 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Eloquent\\Builder.php(206): Illuminate\\Database\\Eloquent\\Builder->get(Array)\n#10 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\plugins\\raviraj\\rjsliders\\components\\Advanced.php(312): Illuminate\\Database\\Eloquent\\Builder->first()\n#11 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\modules\\cms\\classes\\Controller.php(988): Raviraj\\Rjsliders\\Components\\Advanced->onRender()\n#12 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\modules\\cms\\twig\\Extension.php(139): Cms\\Classes\\Controller->renderComponent(\'home_slider\', Array)\n#13 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\storage\\cms\\twig\\ca\\ca77026aa28a68e00b57ff1b76b27e8da45469057090847a0abebd8de47b2e51.php(20): Cms\\Twig\\Extension->componentFunction(\'home_slider\', Array)\n#14 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\twig\\twig\\lib\\Twig\\Template.php(396): __TwigTemplate_9497cd7253c40e6fd16696f1adbbbd755073c098751e4481c16391e6fc10bcc7->doDisplay(Array, Array)\n#15 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\twig\\twig\\lib\\Twig\\Template.php(364): Twig_Template->displayWithErrorHandling(Array, Array)\n#16 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\twig\\twig\\lib\\Twig\\Template.php(375): Twig_Template->display(Array)\n#17 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\modules\\cms\\classes\\Controller.php(377): Twig_Template->render(Array)\n#18 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\modules\\cms\\classes\\Controller.php(215): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#19 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\modules\\cms\\classes\\CmsController.php(48): Cms\\Classes\\Controller->run(\'/\')\n#20 [internal function]: Cms\\Classes\\CmsController->run(\'/\')\n#21 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Controller.php(256): call_user_func_array(Array, Array)\n#22 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\ControllerDispatcher.php(164): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#23 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\ControllerDispatcher.php(112): Illuminate\\Routing\\ControllerDispatcher->call(Object(Cms\\Classes\\CmsController), Object(Illuminate\\Routing\\Route), \'run\')\n#24 [internal function]: Illuminate\\Routing\\ControllerDispatcher->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#25 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(139): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#26 [internal function]: Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#27 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(102): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#28 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\ControllerDispatcher.php(114): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#29 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\ControllerDispatcher.php(67): Illuminate\\Routing\\ControllerDispatcher->callWithinStack(Object(Cms\\Classes\\CmsController), Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request), \'run\')\n#30 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Route.php(203): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request), \'Cms\\\\Classes\\\\Cms...\', \'run\')\n#31 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Route.php(134): Illuminate\\Routing\\Route->runWithCustomDispatcher(Object(Illuminate\\Http\\Request))\n#32 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(708): Illuminate\\Routing\\Route->run(Object(Illuminate\\Http\\Request))\n#33 [internal function]: Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#34 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(139): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#35 [internal function]: Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#36 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(102): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#37 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(710): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#38 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(673): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#39 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(635): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#40 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(236): Illuminate\\Routing\\Router->dispatch(Object(Illuminate\\Http\\Request))\n#41 [internal function]: Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#42 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(139): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#43 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\View\\Middleware\\ShareErrorsFromSession.php(49): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#44 [internal function]: Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#45 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(124): call_user_func_array(Array, Array)\n#46 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Session\\Middleware\\StartSession.php(62): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#47 [internal function]: Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#48 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(124): call_user_func_array(Array, Array)\n#49 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse.php(37): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#50 [internal function]: Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#51 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(124): call_user_func_array(Array, Array)\n#52 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Cookie\\Middleware\\EncryptCookies.php(59): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#53 [internal function]: Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#54 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(124): call_user_func_array(Array, Array)\n#55 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode.php(44): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#56 [internal function]: Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#57 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(124): call_user_func_array(Array, Array)\n#58 [internal function]: Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#59 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(102): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#60 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(122): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#61 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(87): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#62 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#63 {main}\n\nNext Illuminate\\Database\\QueryException: SQLSTATE[42S02]: Base table or view not found: 1146 Table \'octobercms_angularjs.raviraj_rjsliders_sliders\' doesn\'t exist (SQL: select * from `raviraj_rjsliders_sliders` where `id` = 1 limit 1) in C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php:662\nStack trace:\n#0 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(618): Illuminate\\Database\\Connection->runQueryCallback(\'select * from `...\', Array, Object(Closure))\n#1 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(324): Illuminate\\Database\\Connection->run(\'select * from `...\', Array, Object(Closure))\n#2 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Query\\Builder.php(1431): Illuminate\\Database\\Connection->select(\'select * from `...\', Array, true)\n#3 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Query\\Builder.php(1408): Illuminate\\Database\\Query\\Builder->runSelect()\n#4 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\october\\rain\\src\\Database\\QueryBuilder.php(76): Illuminate\\Database\\Query\\Builder->get(Array)\n#5 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Eloquent\\Builder.php(477): October\\Rain\\Database\\QueryBuilder->get(Array)\n#6 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Eloquent\\Builder.php(234): Illuminate\\Database\\Eloquent\\Builder->getModels(Array)\n#7 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Eloquent\\Builder.php(206): Illuminate\\Database\\Eloquent\\Builder->get(Array)\n#8 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\plugins\\raviraj\\rjsliders\\components\\Advanced.php(312): Illuminate\\Database\\Eloquent\\Builder->first()\n#9 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\modules\\cms\\classes\\Controller.php(988): Raviraj\\Rjsliders\\Components\\Advanced->onRender()\n#10 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\modules\\cms\\twig\\Extension.php(139): Cms\\Classes\\Controller->renderComponent(\'home_slider\', Array)\n#11 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\storage\\cms\\twig\\ca\\ca77026aa28a68e00b57ff1b76b27e8da45469057090847a0abebd8de47b2e51.php(20): Cms\\Twig\\Extension->componentFunction(\'home_slider\', Array)\n#12 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\twig\\twig\\lib\\Twig\\Template.php(396): __TwigTemplate_9497cd7253c40e6fd16696f1adbbbd755073c098751e4481c16391e6fc10bcc7->doDisplay(Array, Array)\n#13 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\twig\\twig\\lib\\Twig\\Template.php(364): Twig_Template->displayWithErrorHandling(Array, Array)\n#14 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\twig\\twig\\lib\\Twig\\Template.php(375): Twig_Template->display(Array)\n#15 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\modules\\cms\\classes\\Controller.php(377): Twig_Template->render(Array)\n#16 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\modules\\cms\\classes\\Controller.php(215): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#17 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\modules\\cms\\classes\\CmsController.php(48): Cms\\Classes\\Controller->run(\'/\')\n#18 [internal function]: Cms\\Classes\\CmsController->run(\'/\')\n#19 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Controller.php(256): call_user_func_array(Array, Array)\n#20 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\ControllerDispatcher.php(164): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#21 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\ControllerDispatcher.php(112): Illuminate\\Routing\\ControllerDispatcher->call(Object(Cms\\Classes\\CmsController), Object(Illuminate\\Routing\\Route), \'run\')\n#22 [internal function]: Illuminate\\Routing\\ControllerDispatcher->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#23 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(139): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#24 [internal function]: Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#25 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(102): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#26 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\ControllerDispatcher.php(114): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#27 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\ControllerDispatcher.php(67): Illuminate\\Routing\\ControllerDispatcher->callWithinStack(Object(Cms\\Classes\\CmsController), Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request), \'run\')\n#28 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Route.php(203): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request), \'Cms\\\\Classes\\\\Cms...\', \'run\')\n#29 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Route.php(134): Illuminate\\Routing\\Route->runWithCustomDispatcher(Object(Illuminate\\Http\\Request))\n#30 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(708): Illuminate\\Routing\\Route->run(Object(Illuminate\\Http\\Request))\n#31 [internal function]: Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#32 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(139): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#33 [internal function]: Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#34 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(102): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#35 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(710): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#36 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(673): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#37 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(635): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#38 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(236): Illuminate\\Routing\\Router->dispatch(Object(Illuminate\\Http\\Request))\n#39 [internal function]: Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#40 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(139): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#41 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\View\\Middleware\\ShareErrorsFromSession.php(49): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#42 [internal function]: Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#43 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(124): call_user_func_array(Array, Array)\n#44 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Session\\Middleware\\StartSession.php(62): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#45 [internal function]: Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#46 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(124): call_user_func_array(Array, Array)\n#47 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse.php(37): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#48 [internal function]: Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#49 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(124): call_user_func_array(Array, Array)\n#50 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Cookie\\Middleware\\EncryptCookies.php(59): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#51 [internal function]: Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#52 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(124): call_user_func_array(Array, Array)\n#53 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode.php(44): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#54 [internal function]: Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#55 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(124): call_user_func_array(Array, Array)\n#56 [internal function]: Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#57 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(102): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#58 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(122): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#59 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(87): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#60 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#61 {main}\n\nNext Twig_Error_Runtime: An exception has been thrown during the rendering of a template ("SQLSTATE[42S02]: Base table or view not found: 1146 Table \'octobercms_angularjs.raviraj_rjsliders_sliders\' doesn\'t exist (SQL: select * from `raviraj_rjsliders_sliders` where `id` = 1 limit 1)") in "C:\\wamp64\\www\\NeonMobile\\dealvine-website/themes/demo/pages/home.htm" at line 1. in C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\twig\\twig\\lib\\Twig\\Template.php:411\nStack trace:\n#0 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\twig\\twig\\lib\\Twig\\Template.php(364): Twig_Template->displayWithErrorHandling(Array, Array)\n#1 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\twig\\twig\\lib\\Twig\\Template.php(375): Twig_Template->display(Array)\n#2 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\modules\\cms\\classes\\Controller.php(377): Twig_Template->render(Array)\n#3 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\modules\\cms\\classes\\Controller.php(215): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#4 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\modules\\cms\\classes\\CmsController.php(48): Cms\\Classes\\Controller->run(\'/\')\n#5 [internal function]: Cms\\Classes\\CmsController->run(\'/\')\n#6 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Controller.php(256): call_user_func_array(Array, Array)\n#7 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\ControllerDispatcher.php(164): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#8 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\ControllerDispatcher.php(112): Illuminate\\Routing\\ControllerDispatcher->call(Object(Cms\\Classes\\CmsController), Object(Illuminate\\Routing\\Route), \'run\')\n#9 [internal function]: Illuminate\\Routing\\ControllerDispatcher->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#10 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(139): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#11 [internal function]: Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#12 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(102): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#13 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\ControllerDispatcher.php(114): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#14 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\ControllerDispatcher.php(67): Illuminate\\Routing\\ControllerDispatcher->callWithinStack(Object(Cms\\Classes\\CmsController), Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request), \'run\')\n#15 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Route.php(203): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request), \'Cms\\\\Classes\\\\Cms...\', \'run\')\n#16 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Route.php(134): Illuminate\\Routing\\Route->runWithCustomDispatcher(Object(Illuminate\\Http\\Request))\n#17 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(708): Illuminate\\Routing\\Route->run(Object(Illuminate\\Http\\Request))\n#18 [internal function]: Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#19 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(139): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#20 [internal function]: Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#21 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(102): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#22 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(710): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#23 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(673): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#24 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(635): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#25 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(236): Illuminate\\Routing\\Router->dispatch(Object(Illuminate\\Http\\Request))\n#26 [internal function]: Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#27 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(139): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#28 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\View\\Middleware\\ShareErrorsFromSession.php(49): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#29 [internal function]: Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#30 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(124): call_user_func_array(Array, Array)\n#31 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Session\\Middleware\\StartSession.php(62): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#32 [internal function]: Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#33 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(124): call_user_func_array(Array, Array)\n#34 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse.php(37): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#35 [internal function]: Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#36 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(124): call_user_func_array(Array, Array)\n#37 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Cookie\\Middleware\\EncryptCookies.php(59): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#38 [internal function]: Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#39 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(124): call_user_func_array(Array, Array)\n#40 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode.php(44): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#41 [internal function]: Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#42 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(124): call_user_func_array(Array, Array)\n#43 [internal function]: Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#44 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(102): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#45 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(122): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#46 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(87): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#47 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#48 {main}', NULL, '2016-12-11 05:10:53', '2016-12-11 05:10:53'),
(5, 'error', 'RuntimeException: Failed to write cache file "C:\\wamp64\\www\\NeonMobile\\dealvine-website\\storage/cms/twig/4e/4e89debd4b23ebf2590a3c116d7ae6859465e4a3a0478912c9ebf2470ec5a28c.php". in C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\twig\\twig\\lib\\Twig\\Cache\\Filesystem.php:84\nStack trace:\n#0 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\twig\\twig\\lib\\Twig\\Environment.php(448): Twig_Cache_Filesystem->write(\'C:\\\\wamp64\\\\www\\\\N...\', \'<?php\\n\\n/* C:\\\\wa...\')\n#1 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\modules\\cms\\classes\\Controller.php(377): Twig_Environment->loadTemplate(\'C:\\\\wamp64\\\\www\\\\N...\')\n#2 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\modules\\cms\\classes\\Controller.php(216): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#3 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\modules\\cms\\classes\\CmsController.php(48): Cms\\Classes\\Controller->run(\'themes/dealvine...\')\n#4 [internal function]: Cms\\Classes\\CmsController->run(\'themes/dealvine...\')\n#5 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Controller.php(256): call_user_func_array(Array, Array)\n#6 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\ControllerDispatcher.php(164): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#7 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\ControllerDispatcher.php(112): Illuminate\\Routing\\ControllerDispatcher->call(Object(Cms\\Classes\\CmsController), Object(Illuminate\\Routing\\Route), \'run\')\n#8 [internal function]: Illuminate\\Routing\\ControllerDispatcher->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#9 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(139): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#10 [internal function]: Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#11 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(102): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#12 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\ControllerDispatcher.php(114): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#13 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\ControllerDispatcher.php(67): Illuminate\\Routing\\ControllerDispatcher->callWithinStack(Object(Cms\\Classes\\CmsController), Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request), \'run\')\n#14 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Route.php(203): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request), \'Cms\\\\Classes\\\\Cms...\', \'run\')\n#15 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Route.php(134): Illuminate\\Routing\\Route->runWithCustomDispatcher(Object(Illuminate\\Http\\Request))\n#16 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(708): Illuminate\\Routing\\Route->run(Object(Illuminate\\Http\\Request))\n#17 [internal function]: Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#18 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(139): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#19 [internal function]: Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#20 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(102): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#21 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(710): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#22 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(673): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#23 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(635): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#24 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(236): Illuminate\\Routing\\Router->dispatch(Object(Illuminate\\Http\\Request))\n#25 [internal function]: Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#26 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(139): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#27 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\View\\Middleware\\ShareErrorsFromSession.php(49): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#28 [internal function]: Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#29 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(124): call_user_func_array(Array, Array)\n#30 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Session\\Middleware\\StartSession.php(62): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#31 [internal function]: Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#32 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(124): call_user_func_array(Array, Array)\n#33 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse.php(37): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#34 [internal function]: Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#35 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(124): call_user_func_array(Array, Array)\n#36 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Cookie\\Middleware\\EncryptCookies.php(59): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#37 [internal function]: Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#38 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(124): call_user_func_array(Array, Array)\n#39 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode.php(44): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#40 [internal function]: Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#41 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(124): call_user_func_array(Array, Array)\n#42 [internal function]: Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#43 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(102): call_user_func(Object(Closure), Object(Illuminate\\Http\\Request))\n#44 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(122): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#45 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(87): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#46 C:\\wamp64\\www\\NeonMobile\\dealvine-website\\index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#47 {main}', NULL, '2016-12-11 05:26:54', '2016-12-11 05:26:54');

-- --------------------------------------------------------

--
-- Table structure for table `system_files`
--

CREATE TABLE `system_files` (
  `id` int(10) UNSIGNED NOT NULL,
  `disk_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file_size` int(11) NOT NULL,
  `content_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `field` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attachment_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attachment_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_public` tinyint(1) NOT NULL DEFAULT '1',
  `sort_order` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_mail_layouts`
--

CREATE TABLE `system_mail_layouts` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content_html` text COLLATE utf8_unicode_ci,
  `content_text` text COLLATE utf8_unicode_ci,
  `content_css` text COLLATE utf8_unicode_ci,
  `is_locked` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `system_mail_layouts`
--

INSERT INTO `system_mail_layouts` (`id`, `name`, `code`, `content_html`, `content_text`, `content_css`, `is_locked`, `created_at`, `updated_at`) VALUES
(1, 'Default', 'default', '<html>\n    <head>\n        <style type="text/css" media="screen">\n            {{ css|raw }}\n        </style>\n    </head>\n    <body>\n        {{ content|raw }}\n    </body>\n</html>', '{{ content|raw }}', 'a, a:hover {\n    text-decoration: none;\n    color: #0862A2;\n    font-weight: bold;\n}\n\ntd, tr, th, table {\n    padding: 0px;\n    margin: 0px;\n}\n\np {\n    margin: 10px 0;\n}', 1, '2016-11-28 00:29:35', '2016-11-28 00:29:35'),
(2, 'System', 'system', '<html>\n    <head>\n        <style type="text/css" media="screen">\n            {{ css|raw }}\n        </style>\n    </head>\n    <body>\n        {{ content|raw }}\n        <hr />\n        <p>This is an automatic message. Please do not reply to it.</p>\n    </body>\n</html>', '{{ content|raw }}\n\n\n---\nThis is an automatic message. Please do not reply to it.', 'a, a:hover {\n    text-decoration: none;\n    color: #0862A2;\n    font-weight: bold;\n}\n\ntd, tr, th, table {\n    padding: 0px;\n    margin: 0px;\n}\n\np {\n    margin: 10px 0;\n}', 1, '2016-11-28 00:29:35', '2016-11-28 00:29:35');

-- --------------------------------------------------------

--
-- Table structure for table `system_mail_templates`
--

CREATE TABLE `system_mail_templates` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `content_html` text COLLATE utf8_unicode_ci,
  `content_text` text COLLATE utf8_unicode_ci,
  `layout_id` int(11) DEFAULT NULL,
  `is_custom` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_parameters`
--

CREATE TABLE `system_parameters` (
  `id` int(10) UNSIGNED NOT NULL,
  `namespace` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `group` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `item` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `system_parameters`
--

INSERT INTO `system_parameters` (`id`, `namespace`, `group`, `item`, `value`) VALUES
(1, 'system', 'update', 'count', '1'),
(2, 'system', 'update', 'retry', '1480495849'),
(3, 'cms', 'theme', 'active', '"dealvine"');

-- --------------------------------------------------------

--
-- Table structure for table `system_plugin_history`
--

CREATE TABLE `system_plugin_history` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `version` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `detail` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `system_plugin_history`
--

INSERT INTO `system_plugin_history` (`id`, `code`, `type`, `version`, `detail`, `created_at`) VALUES
(1, 'October.Demo', 'comment', '1.0.1', 'First version of Demo', '2016-11-28 00:29:35');

-- --------------------------------------------------------

--
-- Table structure for table `system_plugin_versions`
--

CREATE TABLE `system_plugin_versions` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `version` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `is_disabled` tinyint(1) NOT NULL DEFAULT '0',
  `is_frozen` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `system_plugin_versions`
--

INSERT INTO `system_plugin_versions` (`id`, `code`, `version`, `created_at`, `is_disabled`, `is_frozen`) VALUES
(1, 'October.Demo', '1.0.1', '2016-11-28 00:29:35', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `system_request_logs`
--

CREATE TABLE `system_request_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `status_code` int(11) DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `referer` text COLLATE utf8_unicode_ci,
  `count` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `system_request_logs`
--

INSERT INTO `system_request_logs` (`id`, `status_code`, `url`, `referer`, `count`, `created_at`, `updated_at`) VALUES
(1, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine/october-angularjs/themes/dealvine/assets/components/angularjs/angular.min.js', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine\\/october-angularjs\\/"]', 9, '2016-11-28 01:21:31', '2016-11-28 01:23:30'),
(2, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine/october-angularjs/themes/dealvine/assets/components/angular-ui-router/releases/angular-ui-router.min.js', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine\\/october-angularjs\\/"]', 11, '2016-11-28 01:21:31', '2016-11-28 01:23:30'),
(3, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine/october-angularjs/hello', NULL, 1, '2016-11-28 01:22:24', '2016-11-28 01:22:24'),
(4, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine/october-angularjs/themes/dealvine/assets', NULL, 1, '2016-11-28 01:23:29', '2016-11-28 01:23:29'),
(5, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine/october-angularjs/themes/dealvine/assets/components/angular-touch/angular-touch.min.min.js', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine\\/october-angularjs\\/"]', 4, '2016-11-28 01:52:02', '2016-11-28 01:53:32'),
(6, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine/october-angularjs/themes/dealvine/assets/components/angular-animate/angular-animate.min.min.js', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine\\/october-angularjs\\/"]', 4, '2016-11-28 01:52:02', '2016-11-28 01:53:32'),
(7, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine/october-angularjs/templates/home.html', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine\\/october-angularjs\\/"]', 109, '2016-11-28 01:52:05', '2016-11-28 01:53:14'),
(8, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine/october-angularjs/themes/dealvine/assets/images/favicon.png', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine\\/october-angularjs\\/"]', 1, '2016-11-28 01:52:05', '2016-11-28 01:52:05'),
(9, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine/october-angularjs/themes/dealvine/assets/images/favicon.png', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine\\/october-angularjs\\/"]', 1, '2016-11-28 01:52:05', '2016-11-28 01:52:05'),
(10, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine/october-angularjs/themes/dealvine/assets/images/dealvine%20logo.png', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine\\/october-angularjs\\/"]', 2, '2016-11-28 02:03:49', '2016-11-28 02:05:56'),
(11, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine/october-angularjs/themes/dealvine/assets/images/search.png', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine\\/october-angularjs\\/"]', 1, '2016-11-28 02:03:49', '2016-11-28 02:03:49'),
(12, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine/october-angularjs/themes/dealvine/assets/images/apple.png', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine\\/october-angularjs\\/"]', 1, '2016-11-28 02:05:57', '2016-11-28 02:05:57'),
(13, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine/october-angularjs/themes/dealvine/assets/images/googleapp.png', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine\\/october-angularjs\\/"]', 1, '2016-11-28 02:05:57', '2016-11-28 02:05:57'),
(14, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine/october-angularjs/themes/dealvine/assets/controllers/homeController.js', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine\\/october-angularjs\\/"]', 4, '2016-11-28 02:23:10', '2016-11-28 02:23:57'),
(15, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine/october-angularjs/%7B%7B%20\'assets/images/ellipse.png\'%7Ctheme%20%7D%7D', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine\\/october-angularjs\\/"]', 1, '2016-11-28 04:20:54', '2016-11-28 04:20:54'),
(16, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine/october-angularjs/assets/images/ellipse.png', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine\\/october-angularjs\\/"]', 1, '2016-11-28 04:21:36', '2016-11-28 04:21:36'),
(17, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine/october-angularjs/theme/dealvine/assets/images/ellipse.png', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine\\/october-angularjs\\/"]', 1, '2016-11-28 04:21:59', '2016-11-28 04:21:59'),
(18, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine/october-angularjs/themes/dealvine/assets/images/right.png', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine\\/october-angularjs\\/"]', 1, '2016-11-28 05:20:58', '2016-11-28 05:20:58'),
(19, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine/october-angularjs/themes/dealvine/assets/images/left.png', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine\\/october-angularjs\\/"]', 1, '2016-11-28 05:20:59', '2016-11-28 05:20:59'),
(20, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine/october-angularjs/themes/demo/assets/images/wellness.png', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine\\/october-angularjs\\/"]', 1, '2016-11-28 05:21:01', '2016-11-28 05:21:01'),
(21, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine/october-angularjs/themes/dealvine/assets/app/services/homeServie.js', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine\\/october-angularjs\\/"]', 2, '2016-11-28 05:55:15', '2016-11-28 05:55:16'),
(22, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine-website/themes/dealvine/assets/images/favicon.png', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine-website\\/"]', 3, '2016-11-29 01:02:52', '2016-11-29 01:08:26'),
(23, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine-website/themes/dealvine/assets/app/templates/home.html', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine-website\\/"]', 8, '2016-11-29 01:24:24', '2016-11-29 01:25:03'),
(24, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine-website/assets/images/right.png', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine-website\\/"]', 1, '2016-11-29 01:27:31', '2016-11-29 01:27:31'),
(25, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine-website/assets/images/app_store.png', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine-website\\/"]', 2, '2016-11-29 01:27:31', '2016-11-29 01:28:13'),
(26, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine-website/assets/images/left.png', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine-website\\/"]', 1, '2016-11-29 01:27:31', '2016-11-29 01:27:31'),
(27, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine-website/assets/images/google_store.png', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine-website\\/"]', 2, '2016-11-29 01:27:31', '2016-11-29 01:28:13'),
(28, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine-website/assets/images/phone.png', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine-website\\/"]', 2, '2016-11-29 01:27:31', '2016-11-29 01:28:13'),
(29, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine-website/assets/images/discount_back.png', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine-website\\/"]', 2, '2016-11-29 01:27:31', '2016-11-29 01:28:13'),
(30, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine-website/assets/images/save_20.png', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine-website\\/"]', 1, '2016-11-29 03:59:28', '2016-11-29 03:59:28'),
(31, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine-website/themes/dealvine/assets/images/back-icon.png', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine-website\\/themes\\/dealvine\\/assets\\/css\\/style.css"]', 1, '2016-11-29 03:59:29', '2016-11-29 03:59:29'),
(32, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine-website/themes/dealvine/assets/images/tags.png', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine-website\\/"]', 2, '2016-11-29 03:59:29', '2016-11-29 04:02:53'),
(33, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine-website/themes/dealvine/assets/app/directives/store_category_brand.html', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine-website\\/"]', 1, '2016-11-29 04:29:10', '2016-11-29 04:29:10'),
(34, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine-website/themes/dealvine/assets/components/slick-carousel/images/left.png', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine-website\\/themes\\/dealvine\\/assets\\/components\\/slick-carousel\\/slick\\/slick-theme.css"]', 11, '2016-11-29 05:34:01', '2016-11-29 05:39:05'),
(35, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine-website/themes/dealvine/assets/components/slick-carousel/images/themes/dealvine/assets/app/templates/home.html', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine-website\\/themes\\/dealvine\\/assets\\/components\\/slick-carousel\\/images\\/left.png"]', 1, '2016-11-29 05:34:58', '2016-11-29 05:34:58'),
(36, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine-website/themes/dealvine/assets/components/slick-carousel/.images/left.png', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine-website\\/themes\\/dealvine\\/assets\\/components\\/slick-carousel\\/slick\\/slick-theme.css"]', 1, '2016-11-29 05:36:02', '2016-11-29 05:36:02'),
(37, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine-website/themes/dealvine/assets/components/slick-carousel/..images/left.png', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine-website\\/themes\\/dealvine\\/assets\\/components\\/slick-carousel\\/slick\\/slick-theme.css"]', 1, '2016-11-29 05:36:02', '2016-11-29 05:36:02'),
(38, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine-website/themes/dealvine/assets/components/images/left.png', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine-website\\/themes\\/dealvine\\/assets\\/components\\/slick-carousel\\/slick\\/slick-theme.css"]', 1, '2016-11-29 05:36:02', '2016-11-29 05:36:02'),
(39, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine-website/themes/dealvine/assets/components/.images/left.png', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine-website\\/themes\\/dealvine\\/assets\\/components\\/slick-carousel\\/slick\\/slick-theme.css"]', 1, '2016-11-29 05:36:03', '2016-11-29 05:36:03'),
(40, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine-website/themes/dealvine/assets/components/..images/left.png', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine-website\\/themes\\/dealvine\\/assets\\/components\\/slick-carousel\\/slick\\/slick-theme.css"]', 1, '2016-11-29 05:36:03', '2016-11-29 05:36:03'),
(41, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine-website/themes/dealvine/assets/.images/left.png', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine-website\\/themes\\/dealvine\\/assets\\/components\\/slick-carousel\\/slick\\/slick-theme.css"]', 2, '2016-11-29 05:36:03', '2016-11-29 05:36:05'),
(42, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine-website/themes/dealvine/images/left.png', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine-website\\/themes\\/dealvine\\/assets\\/components\\/slick-carousel\\/slick\\/slick-theme.css"]', 1, '2016-11-29 05:36:03', '2016-11-29 05:36:03'),
(43, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine-website/themes/dealvine/assets/..images/left.png', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine-website\\/themes\\/dealvine\\/assets\\/components\\/slick-carousel\\/slick\\/slick-theme.css"]', 2, '2016-11-29 05:36:03', '2016-11-29 05:36:04'),
(44, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine-website/themes/dealvine/assets/images/r.png', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine-website\\/themes\\/dealvine\\/assets\\/components\\/slick-carousel\\/slick\\/slick-theme.css"]', 1, '2016-11-29 05:36:08', '2016-11-29 05:36:08'),
(45, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine-website/themes/dealvine/assets/images/ri.png', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine-website\\/themes\\/dealvine\\/assets\\/components\\/slick-carousel\\/slick\\/slick-theme.css"]', 1, '2016-11-29 05:36:08', '2016-11-29 05:36:08'),
(46, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine-website/themes/dealvine/assets/images/rig.png', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine-website\\/themes\\/dealvine\\/assets\\/components\\/slick-carousel\\/slick\\/slick-theme.css"]', 1, '2016-11-29 05:36:08', '2016-11-29 05:36:08'),
(47, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine-website/themes/dealvine/assets/images/righ.png', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine-website\\/themes\\/dealvine\\/assets\\/components\\/slick-carousel\\/slick\\/slick-theme.css"]', 1, '2016-11-29 05:36:08', '2016-11-29 05:36:08'),
(48, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine-website/themes/dealvine/assets/components/slick-carousel/images/right.png', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine-website\\/themes\\/dealvine\\/assets\\/components\\/slick-carousel\\/slick\\/slick-theme.css"]', 2, '2016-11-29 05:38:44', '2016-11-29 05:38:55'),
(49, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine-website/themes/dealvine/assets/components/slick-carousel', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine-website\\/themes\\/dealvine\\/assets\\/components\\/slick-carousel\\/slick\\/slick-theme.css"]', 1, '2016-11-29 05:43:40', '2016-11-29 05:43:40'),
(50, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine-website/themes/dealvine/assets/components/slick-carousel/slick', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine-website\\/themes\\/dealvine\\/assets\\/components\\/slick-carousel\\/slick\\/slick-theme.css"]', 1, '2016-11-29 05:43:40', '2016-11-29 05:43:40'),
(51, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine-website/themes/dealvine/assets/components', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine-website\\/themes\\/dealvine\\/assets\\/components\\/slick-carousel\\/slick\\/slick-theme.css"]', 1, '2016-11-29 05:43:40', '2016-11-29 05:43:40'),
(52, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine-website/themes/dealvine/assets', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine-website\\/themes\\/dealvine\\/assets\\/components\\/slick-carousel\\/slick\\/slick-theme.css"]', 1, '2016-11-29 05:43:41', '2016-11-29 05:43:41'),
(53, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine-website/themes/dealvine/assets/i', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine-website\\/themes\\/dealvine\\/assets\\/components\\/slick-carousel\\/slick\\/slick-theme.css"]', 1, '2016-11-29 05:43:42', '2016-11-29 05:43:42'),
(54, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine-website/themes/dealvine/assets/ima', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine-website\\/themes\\/dealvine\\/assets\\/components\\/slick-carousel\\/slick\\/slick-theme.css"]', 2, '2016-11-29 05:43:42', '2016-11-29 05:43:43'),
(55, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine-website/themes/dealvine/assets/im', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine-website\\/themes\\/dealvine\\/assets\\/components\\/slick-carousel\\/slick\\/slick-theme.css"]', 1, '2016-11-29 05:43:42', '2016-11-29 05:43:42'),
(56, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine-website/themes/dealvine/assets/imaeg', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine-website\\/themes\\/dealvine\\/assets\\/components\\/slick-carousel\\/slick\\/slick-theme.css"]', 1, '2016-11-29 05:43:42', '2016-11-29 05:43:42'),
(57, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine-website/themes/dealvine/assets/imae', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine-website\\/themes\\/dealvine\\/assets\\/components\\/slick-carousel\\/slick\\/slick-theme.css"]', 1, '2016-11-29 05:43:43', '2016-11-29 05:43:43'),
(58, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine-website/themes/dealvine/assets/imag', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine-website\\/themes\\/dealvine\\/assets\\/components\\/slick-carousel\\/slick\\/slick-theme.css"]', 1, '2016-11-29 05:43:43', '2016-11-29 05:43:43'),
(59, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine-website/themes/dealvine/assets/image', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine-website\\/themes\\/dealvine\\/assets\\/components\\/slick-carousel\\/slick\\/slick-theme.css"]', 1, '2016-11-29 05:43:43', '2016-11-29 05:43:43'),
(60, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine-website/themes/dealvine/assets/images', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine-website\\/themes\\/dealvine\\/assets\\/components\\/slick-carousel\\/slick\\/slick-theme.css"]', 2, '2016-11-29 05:43:43', '2016-11-29 05:43:44'),
(61, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine-website/themes/dealvine/assets/images/l', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine-website\\/themes\\/dealvine\\/assets\\/components\\/slick-carousel\\/slick\\/slick-theme.css"]', 1, '2016-11-29 05:43:45', '2016-11-29 05:43:45'),
(62, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine-website/themes/dealvine/assets/images/le', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine-website\\/themes\\/dealvine\\/assets\\/components\\/slick-carousel\\/slick\\/slick-theme.css"]', 1, '2016-11-29 05:43:45', '2016-11-29 05:43:45'),
(63, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine-website/themes/dealvine/assets/images/lef', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine-website\\/themes\\/dealvine\\/assets\\/components\\/slick-carousel\\/slick\\/slick-theme.css"]', 1, '2016-11-29 05:43:45', '2016-11-29 05:43:45'),
(64, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine-website/themes/dealvine/assets/images/left', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine-website\\/themes\\/dealvine\\/assets\\/components\\/slick-carousel\\/slick\\/slick-theme.css"]', 1, '2016-11-29 05:43:46', '2016-11-29 05:43:46'),
(65, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine-website/themes/dealvine/assets/images/left.', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine-website\\/themes\\/dealvine\\/assets\\/components\\/slick-carousel\\/slick\\/slick-theme.css"]', 1, '2016-11-29 05:43:46', '2016-11-29 05:43:46'),
(66, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine-website/themes/dealvine/assets/images/left.p', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine-website\\/themes\\/dealvine\\/assets\\/components\\/slick-carousel\\/slick\\/slick-theme.css"]', 1, '2016-11-29 05:43:46', '2016-11-29 05:43:46'),
(67, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine-website/themes/dealvine/assets/images/left.pn', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine-website\\/themes\\/dealvine\\/assets\\/components\\/slick-carousel\\/slick\\/slick-theme.css"]', 1, '2016-11-29 05:43:46', '2016-11-29 05:43:46'),
(68, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine-website/stores/hellofresh', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine-website\\/"]', 1, '2016-11-29 06:09:51', '2016-11-29 06:09:51'),
(69, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine-website/stores/themes/dealvine/assets/app/templates/home.html', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine-website\\/stores\\/hellofresh"]', 1, '2016-11-29 06:09:57', '2016-11-29 06:09:57'),
(70, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine-website-only-octobercms', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/"]', 1, '2016-12-04 23:10:15', '2016-12-04 23:10:15'),
(71, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine-website/themes/dealvine/assets/images/travels.png', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine-website\\/"]', 4, '2016-12-05 03:48:47', '2016-12-05 03:50:32'),
(72, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine-website/%7B%7B%20savingCard.image%20%7D%7D', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine-website\\/"]', 14, '2016-12-05 03:50:33', '2016-12-05 04:33:28'),
(73, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine-website/category-deals/fashion', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine-website\\/"]', 1, '2016-12-06 05:07:50', '2016-12-06 05:07:50'),
(74, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine-website/category-deals/themes/dealvine/assets/app/templates/home.html', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine-website\\/category-deals\\/fashion"]', 1, '2016-12-06 05:07:52', '2016-12-06 05:07:52'),
(75, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine-website/themes/dealvine/assets/components/angular-payments/angular-sanitize.min.js', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine-website\\/"]', 1, '2016-12-06 07:00:30', '2016-12-06 07:00:30'),
(76, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine-website/themes/dealvine/assets/ap/templates/stripe_payment.html', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine-website\\/"]', 2, '2016-12-06 07:40:26', '2016-12-06 07:40:28'),
(77, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine-website/themes/dealvine/assets/components/underscore/underscore.min.js', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine-website\\/"]', 3, '2016-12-07 12:45:25', '2016-12-07 12:45:41'),
(78, 404, 'http://localhost/MyFreelancing/NeonMobile/dealvine-website/themes/dealvine/assets/app/services/mobileEventsService.js', '["http:\\/\\/localhost\\/MyFreelancing\\/NeonMobile\\/dealvine-website\\/"]', 1, '2016-12-08 08:25:13', '2016-12-08 08:25:13'),
(79, 404, 'http://localhost/NeonMobile/dealvine-website/themes/dealvine/assets/components/sweetalert2/dist/sweetalert2.min.css', '["http:\\/\\/localhost\\/NeonMobile\\/dealvine-website\\/"]', 6, '2016-12-11 05:26:54', '2016-12-11 05:29:11'),
(80, 404, 'http://localhost/NeonMobile/dealvine-website/themes/dealvine/assets/components/slick-carousel/slick/slick-theme.css', '["http:\\/\\/localhost\\/NeonMobile\\/dealvine-website\\/"]', 6, '2016-12-11 05:26:54', '2016-12-11 05:29:11'),
(81, 404, 'http://localhost/NeonMobile/dealvine-website/themes/dealvine/assets/components/bootstrap/dist/css/bootstrap.min.css', '["http:\\/\\/localhost\\/NeonMobile\\/dealvine-website\\/"]', 6, '2016-12-11 05:26:54', '2016-12-11 05:29:11'),
(82, 404, 'http://localhost/NeonMobile/dealvine-website/themes/dealvine/assets/components/slick-carousel/slick/slick.css', '["http:\\/\\/localhost\\/NeonMobile\\/dealvine-website\\/"]', 6, '2016-12-11 05:26:54', '2016-12-11 05:29:11'),
(83, 404, 'http://localhost/NeonMobile/dealvine-website/themes/dealvine/assets/components/jquery/dist/jquery.min.js', '["http:\\/\\/localhost\\/NeonMobile\\/dealvine-website\\/"]', 9, '2016-12-11 05:26:55', '2016-12-11 05:29:14'),
(84, 404, 'http://localhost/NeonMobile/dealvine-website/themes/dealvine/assets/components/bootstrap/dist/js/bootstrap.min.js', '["http:\\/\\/localhost\\/NeonMobile\\/dealvine-website\\/"]', 9, '2016-12-11 05:26:55', '2016-12-11 05:29:15'),
(85, 404, 'http://localhost/NeonMobile/dealvine-website/themes/dealvine/assets/components/angular/angular.min.js', '["http:\\/\\/localhost\\/NeonMobile\\/dealvine-website\\/"]', 10, '2016-12-11 05:26:55', '2016-12-11 05:29:15'),
(86, 404, 'http://localhost/NeonMobile/dealvine-website/themes/dealvine/assets/components/angular-ui-router/release/angular-ui-router.min.js', '["http:\\/\\/localhost\\/NeonMobile\\/dealvine-website\\/"]', 9, '2016-12-11 05:26:55', '2016-12-11 05:29:15'),
(87, 404, 'http://localhost/NeonMobile/dealvine-website/themes/dealvine/assets/components/angular-animate/angular-animate.min.js', '["http:\\/\\/localhost\\/NeonMobile\\/dealvine-website\\/"]', 10, '2016-12-11 05:26:56', '2016-12-11 05:29:15'),
(88, 404, 'http://localhost/NeonMobile/dealvine-website/themes/dealvine/assets/components/angular-touch/angular-touch.min.js', '["http:\\/\\/localhost\\/NeonMobile\\/dealvine-website\\/"]', 10, '2016-12-11 05:26:56', '2016-12-11 05:29:16'),
(89, 404, 'http://localhost/NeonMobile/dealvine-website/themes/dealvine/assets/components/angular-bootstrap/ui-bootstrap-tpls.min.js', '["http:\\/\\/localhost\\/NeonMobile\\/dealvine-website\\/"]', 8, '2016-12-11 05:26:56', '2016-12-11 05:29:16'),
(90, 404, 'http://localhost/NeonMobile/dealvine-website/themes/dealvine/assets/components/slick-carousel/slick/slick.min.js', '["http:\\/\\/localhost\\/NeonMobile\\/dealvine-website\\/"]', 9, '2016-12-11 05:26:56', '2016-12-11 05:29:16'),
(91, 404, 'http://localhost/NeonMobile/dealvine-website/themes/dealvine/assets/components/angular-slick-carousel/dist/angular-slick.min.js', '["http:\\/\\/localhost\\/NeonMobile\\/dealvine-website\\/"]', 8, '2016-12-11 05:26:57', '2016-12-11 05:29:16'),
(92, 404, 'http://localhost/NeonMobile/dealvine-website/themes/dealvine/assets/components/angular-stripe-checkout/angular-stripe-checkout.js', '["http:\\/\\/localhost\\/NeonMobile\\/dealvine-website\\/"]', 8, '2016-12-11 05:26:57', '2016-12-11 05:29:17'),
(93, 404, 'http://localhost/NeonMobile/dealvine-website/themes/dealvine/assets/components/es6-promise/es6-promise.auto.min.js', '["http:\\/\\/localhost\\/NeonMobile\\/dealvine-website\\/"]', 8, '2016-12-11 05:26:57', '2016-12-11 05:29:17'),
(94, 404, 'http://localhost/NeonMobile/dealvine-website/themes/dealvine/assets/components/sweetalert2/dist/sweetalert2.min.js', '["http:\\/\\/localhost\\/NeonMobile\\/dealvine-website\\/"]', 8, '2016-12-11 05:26:57', '2016-12-11 05:29:17'),
(95, 404, 'http://localhost/NeonMobile/dealvine-website/themes/dealvine/assets/components/angular-sanitize/angular-sanitize.min.js', '["http:\\/\\/localhost\\/NeonMobile\\/dealvine-website\\/"]', 9, '2016-12-11 05:26:57', '2016-12-11 05:29:17'),
(96, 404, 'http://localhost/NeonMobile/dealvine-website/themes/dealvine/assets/components/underscore/underscore-min.js', '["http:\\/\\/localhost\\/NeonMobile\\/dealvine-website\\/"]', 7, '2016-12-11 05:26:57', '2016-12-11 05:29:18');

-- --------------------------------------------------------

--
-- Table structure for table `system_revisions`
--

CREATE TABLE `system_revisions` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `field` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cast` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `old_value` text COLLATE utf8_unicode_ci,
  `new_value` text COLLATE utf8_unicode_ci,
  `revisionable_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `revisionable_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_settings`
--

CREATE TABLE `system_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `item` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` mediumtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `backend_access_log`
--
ALTER TABLE `backend_access_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `backend_users`
--
ALTER TABLE `backend_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `login_unique` (`login`),
  ADD UNIQUE KEY `email_unique` (`email`),
  ADD KEY `act_code_index` (`activation_code`),
  ADD KEY `reset_code_index` (`reset_password_code`);

--
-- Indexes for table `backend_users_groups`
--
ALTER TABLE `backend_users_groups`
  ADD PRIMARY KEY (`user_id`,`user_group_id`);

--
-- Indexes for table `backend_user_groups`
--
ALTER TABLE `backend_user_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name_unique` (`name`),
  ADD KEY `code_index` (`code`);

--
-- Indexes for table `backend_user_preferences`
--
ALTER TABLE `backend_user_preferences`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_item_index` (`user_id`,`namespace`,`group`,`item`);

--
-- Indexes for table `backend_user_throttle`
--
ALTER TABLE `backend_user_throttle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `backend_user_throttle_user_id_index` (`user_id`),
  ADD KEY `backend_user_throttle_ip_address_index` (`ip_address`);

--
-- Indexes for table `cache`
--
ALTER TABLE `cache`
  ADD UNIQUE KEY `cache_key_unique` (`key`);

--
-- Indexes for table `cms_theme_data`
--
ALTER TABLE `cms_theme_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cms_theme_data_theme_index` (`theme`);

--
-- Indexes for table `deferred_bindings`
--
ALTER TABLE `deferred_bindings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `deferred_bindings_master_type_index` (`master_type`),
  ADD KEY `deferred_bindings_master_field_index` (`master_field`),
  ADD KEY `deferred_bindings_slave_type_index` (`slave_type`),
  ADD KEY `deferred_bindings_slave_id_index` (`slave_id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD UNIQUE KEY `sessions_id_unique` (`id`);

--
-- Indexes for table `system_event_logs`
--
ALTER TABLE `system_event_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_event_logs_level_index` (`level`);

--
-- Indexes for table `system_files`
--
ALTER TABLE `system_files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_files_field_index` (`field`),
  ADD KEY `system_files_attachment_id_index` (`attachment_id`),
  ADD KEY `system_files_attachment_type_index` (`attachment_type`);

--
-- Indexes for table `system_mail_layouts`
--
ALTER TABLE `system_mail_layouts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_mail_templates`
--
ALTER TABLE `system_mail_templates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_mail_templates_layout_id_index` (`layout_id`);

--
-- Indexes for table `system_parameters`
--
ALTER TABLE `system_parameters`
  ADD PRIMARY KEY (`id`),
  ADD KEY `item_index` (`namespace`,`group`,`item`);

--
-- Indexes for table `system_plugin_history`
--
ALTER TABLE `system_plugin_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_plugin_history_code_index` (`code`),
  ADD KEY `system_plugin_history_type_index` (`type`);

--
-- Indexes for table `system_plugin_versions`
--
ALTER TABLE `system_plugin_versions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_plugin_versions_code_index` (`code`);

--
-- Indexes for table `system_request_logs`
--
ALTER TABLE `system_request_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_revisions`
--
ALTER TABLE `system_revisions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_revisions_revisionable_id_revisionable_type_index` (`revisionable_id`,`revisionable_type`),
  ADD KEY `system_revisions_user_id_index` (`user_id`),
  ADD KEY `system_revisions_field_index` (`field`);

--
-- Indexes for table `system_settings`
--
ALTER TABLE `system_settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_settings_item_index` (`item`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `backend_access_log`
--
ALTER TABLE `backend_access_log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `backend_users`
--
ALTER TABLE `backend_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `backend_user_groups`
--
ALTER TABLE `backend_user_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `backend_user_preferences`
--
ALTER TABLE `backend_user_preferences`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `backend_user_throttle`
--
ALTER TABLE `backend_user_throttle`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `cms_theme_data`
--
ALTER TABLE `cms_theme_data`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `deferred_bindings`
--
ALTER TABLE `deferred_bindings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `system_event_logs`
--
ALTER TABLE `system_event_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `system_files`
--
ALTER TABLE `system_files`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `system_mail_layouts`
--
ALTER TABLE `system_mail_layouts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `system_mail_templates`
--
ALTER TABLE `system_mail_templates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `system_parameters`
--
ALTER TABLE `system_parameters`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `system_plugin_history`
--
ALTER TABLE `system_plugin_history`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `system_plugin_versions`
--
ALTER TABLE `system_plugin_versions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `system_request_logs`
--
ALTER TABLE `system_request_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;
--
-- AUTO_INCREMENT for table `system_revisions`
--
ALTER TABLE `system_revisions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `system_settings`
--
ALTER TABLE `system_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
