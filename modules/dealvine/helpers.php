<?php
/**
 * Created by PhpStorm.
 * User: Mitul
 * Date: 14-12-2016
 * Time: 02:07 PM
 */

if (! function_exists('dealvine_elixir')) {
    /**
     * Get the path to a versioned Elixir file for dealvine theme.
     *
     * @param  string $theme
     * @param  string $file
     * @return string
     */
    function dealvine_elixir($theme, $file)
    {
        static $manifest = null;

        if (is_null($manifest)) {
            $fileContents = file_get_contents(base_path('themes/' . $theme . '/assets/build/rev-manifest.json'));
            $manifest = json_decode($fileContents, true);
        }

        if (isset($manifest[$file])) {
            return 'assets/build/'.$manifest[$file];
        }

        throw new InvalidArgumentException("File {$file} not defined in asset manifest.");
    }
}