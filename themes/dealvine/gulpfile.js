const elixir = require('laravel-elixir');
elixir.config.publicPath = './assets';

elixir(function (mix) {
    mix
        .styles([
            './assets/components/bootstrap/dist/css/bootstrap.min.css',
            './assets/components/slick-carousel/slick/slick.css',
            './assets/components/slick-carousel/slick/slick-theme.css',
            './assets/components/sweetalert2/dist/sweetalert2.min.css',
            './assets/components/angular-toastr/dist/angular-toastr.css',
            './assets/components/angular-spinkit/build/angular-spinkit.min.css',
            './assets/css/style.css'
        ], 'assets/build/style.css')
        .scripts([
            "./assets/components/jquery/dist/jquery.min.js",
            "./assets/components/bootstrap/dist/js/bootstrap.min.js",
            "./assets/javascript/wow.min.js",
            "./assets/components/angular/angular.min.js",
            "./assets/components/angular-ui-router/release/angular-ui-router.min.js",
            "./assets/components/angular-animate/angular-animate.min.js",
            "./assets/components/angular-touch/angular-touch.min.js",
            "./assets/components/angular-bootstrap/ui-bootstrap-tpls.min.js",
            "./assets/components/slick-carousel/slick/slick.min.js",
            "./assets/components/angular-slick-carousel/dist/angular-slick.min.js",
            "./assets/components/angular-sanitize/angular-sanitize.min.js",
            "./assets/components/angular-stripe-checkout/angular-stripe-checkout.js",
            "./assets/components/es6-promise/es6-promise.auto.min.js",
            "./assets/components/sweetalert2/dist/sweetalert2.min.js",
            "./assets/components/underscore/underscore-min.js",
            "./assets/components/angular-toastr/dist/angular-toastr.tpls.js",
            "./assets/components/angular-lazy-img/release/angular-lazy-img.min.js",
            "./assets/components/angular-spinkit/build/angular-spinkit.min.js",
            "./assets/components/stacktrace-js/dist/stacktrace.min.js",
            "./assets/components/branch-sdk/dist/build.min.js"
        ], "assets/build/3rd-party.js")
        .copy("assets/images", 'assets/build/images')
        .scripts(["./assets/app/app.js"], "assets/build/app.js")
        .scripts(["./assets/app/js/directives.js"], "assets/build/directives.js")
        .scriptsIn("assets/app/controllers", "assets/build/controllers.js")
        .scriptsIn("assets/app/services", "assets/build/services.js")
        .copy("assets/fonts", 'assets/build/fonts')
        .version([
            'assets/build/style.css',
            'assets/build/3rd-party.js',
            'assets/build/app.js',
            'assets/build/directives.js',
            'assets/build/controllers.js',
            'assets/build/services.js'
        ])
        .browserSync();
});