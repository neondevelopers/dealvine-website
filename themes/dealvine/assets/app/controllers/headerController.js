/**
 * Created by Mitul on 06/12/16.
 */
dealvineApp.controller('HeaderController', [
    '$scope',
    '$rootScope',
    'toastr',
    '$state',
    '$location',
    'CustomerService',
    'DealsService',
    'StoresService',
    'SavingCardsService',
    function ($scope, $rootScope, toastr, $state, $location, CustomerService, DealsService, StoresService, SavingCardsService) {
        $scope.currentSearchRequests = 0;
        $scope.searchText = '';
        $scope.viewAllResultsObj = {
            type: 'View All',
            name: 'All results for ' + $scope.searchText,
            id: -1
        };

        $scope.onLogout = function () {
            window.location.href = baseUrl + 'api/m2/logout?redirectUrl=' + window.encodeURIComponent($location.absUrl());
        };

        $scope.search = function (searchVal) {
            $scope.currentSearchRequests++;
            var promise = DealsService.search(searchVal);
            return promise.then(function (response) {
                $scope.currentSearchRequests--;
                var resData = response.data;
                var searchData = [];
                if (resData.deals) {
                    if (resData.deals.length > 0) {
                        resData.deals[0].hasHeader = true;
                        resData.deals[0].header = "Deals";
                        searchData = searchData.concat(resData.deals.map(function (deal) {
                            deal.type = 'Deal';
                            return deal;
                        }));
                    }
                }
                if (resData.stores) {
                    if (resData.stores.length > 0) {
                        resData.stores[0].hasHeader = true;
                        resData.stores[0].header = "Stores";
                        searchData = searchData.concat(resData.stores.map(function (store) {
                            store.type = 'Store';
                            return store;
                        }));
                    }
                }
                if (resData.savingCards) {
                    if (resData.savingCards.length > 0) {
                        resData.savingCards[0].hasHeader = true;
                        resData.savingCards[0].header = "Saving Cards";
                        searchData = searchData.concat(resData.savingCards.map(function (savingCard) {
                            savingCard.type = 'Saving Card';
                            return savingCard;
                        }));
                    }
                }
                if (searchData.length > 0) {
                    $scope.viewAllResultsObj.name = 'All results for ' + $scope.searchText;
                    searchData.push($scope.viewAllResultsObj);
                }
                return searchData;
            },
            function (error) {
                $scope.currentSearchRequests--;
            });
        };

        $scope.openLoginPopup = function () {
            SavingCardsService.openLoginPopup();
        };

        $scope.goToSearchItem = function ($item, $model, $label) {
            if ($item.type == 'Deal') {
                $state.go('dealDetails', { id: $item.id });
            } else if ($item.type == 'Store') {
                $state.go('storeDeals', { store: $item.mobile_site_name });
            } else if ($item.type == 'Saving Card') {
                $state.go('savingCardDeals', { id: $item.id });
            } else if ($item.type == 'View All') {
                var searchVal = $scope.searchText.name.substr(16);
                $scope.searchText = searchVal;
                $state.go('search', { search: searchVal });
            }
        };

        $scope.onSearch = function () {
            $state.go('search', { search: $scope.searchText });
        };
    }]);