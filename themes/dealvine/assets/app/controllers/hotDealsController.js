/**
 * Created by Mitul on 28/11/16.
 */
dealvineApp.controller('HotDealsController', [
    '$scope', '$rootScope', 'DealsService', '$sce', 'toastr',
    function ($scope, $rootScope, DealsService, $sce, toastr) {
        $scope.hotDeals = [];
        $scope.isLoading = true;
        $scope.noMoreDeals = false;

        $scope.loadMoreDeals = function () {
            retrieveHotDeals();
        };

        function retrieveHotDeals() {
            $scope.isLoading = true;
            DealsService.retrieveHotDeals(10, $scope.hotDeals.length, function (data) {
                $scope.isLoading = false;
                if (data.data.length < 10) {
                    $scope.noMoreDeals = true;
                }
                var newDeals = [];
                data.data.forEach(function (deal) {
                    deal.desc = $sce.trustAsHtml(deal.desc);
                    newDeals.push(deal);
                });
                $scope.hotDeals = $scope.hotDeals.concat(newDeals);
            }, function (error) {
                toastr.error(error.message, 'Error');
            });
        }

        function retrieveData() {
            retrieveHotDeals();
        }

        retrieveData();
    }]);