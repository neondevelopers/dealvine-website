/**
 * Created by Mitul on 28/11/16.
 */
dealvineApp.controller('HomeController', [
    '$scope', '$rootScope', 'HomeService', 'DealsService', 'toastr', 'CustomerService',
    function ($scope, $rootScope, HomeService, DealsService, toastr, CustomerService) {
        $scope.brands = [];
        $scope.flashSales = [];
        $scope.categories = [];
        $scope.hotDeals = [];
        $scope.specialDeals = [];
        $scope.selectedIndex = 0;

        function retrievePopularStores() {
            HomeService.retrievePopularStores(function (data) {
                $scope.brands = data.data;
            }, function (error) {
                toastr.error(error.message, 'Error');
            });
        }

        function retrieveFlashSales() {
            HomeService.retrieveFlashSales(function (data) {
                $scope.flashSales = data.data;
            }, function (error) {
                toastr.error(error.message, 'Error');
            });
        }

        function retrieveCategories() {
            HomeService.retrieveCategories(function (data) {
                data.data.forEach(function (category) {
                    category['slug'] = category.name.replace(/ /g, '-').toLocaleLowerCase();
                    $scope.categories.push(category);
                });

            }, function (error) {
                toastr.error(error.message, 'Error');
            });
        }

        function retrieveHotDeals() {
            DealsService.retrieveHotDeals(4, 0, function (data) {
                $scope.hotDeals = data.data;
            }, function (error) {
                toastr.error(error.message, 'Error');
            });
        }

        function retrieveSpecialDeals() {
            HomeService.retrieveSpecialDeals(function (data) {
                $scope.specialDeals = data.data;
            }, function (error) {
                toastr.error(error.message, 'Error');
            });
        }

        // used for set active class into special deals
        $scope.activateSpecialDeal = function (index) {
            event.preventDefault();
            if (index > $scope.specialDeals.length - 1)
                $scope.selectedIndex = 0;
            else if (index < 0)
                $scope.selectedIndex = $scope.specialDeals.length - 1;
            else
                $scope.selectedIndex = index;
        };

        function retrieveData() {
            retrievePopularStores();
            retrieveFlashSales();
            retrieveCategories();
            retrieveHotDeals();
            retrieveSpecialDeals();
        }

        retrieveData();
    }]);