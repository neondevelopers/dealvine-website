/**
 * Created by Shailesh-Infyom on 10/12/2016.
 */
dealvineApp.controller('SavingCardDealsController', [
    '$scope', '$rootScope', '$stateParams', 'SavingCardsService', 'toastr',
    function ($scope, $rootScope, $stateParams, SavingCardsService, toastr) {
        $scope.savingCard = [];
        $scope.isLoading = true;

        function retrieveSavingCardDeals() {
            SavingCardsService.retrieveSavingCardDeals($stateParams.id, function (data) {
                $scope.savingCard = data.data;
                $scope.isLoading = false;
            }, function (error) {
                toastr.error(error.message, 'Error');
            });
        }

        function retrieveData() {
            retrieveSavingCardDeals();
        }

        $scope.purchaseSavingCard = function () {
            if ($rootScope.isLoggedIn) {
                SavingCardsService.openPurchaseSavingCard($scope.savingCard, $rootScope.customerProfile.id,
                    function (data) {
                        retrieveSavingCardDeals();
                    },
                    function (error) {

                    }
                );
            } else {
                $rootScope.openLoginPopup();
            }
        };

        retrieveData();
    }]);