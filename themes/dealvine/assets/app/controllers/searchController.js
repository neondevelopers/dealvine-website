/**
 * Created by Mitul on 28/11/16.
 */
dealvineApp.controller('SearchController', [
    '$scope', '$rootScope', '$sce', 'toastr', '$stateParams', 'DealsService', 'StoresService', 'SavingCardsService',
    function ($scope, $rootScope, $sce, toastr, $stateParams, DealsService, StoresService, SavingCardsService) {
        $scope.deals = [];
        $scope.savingCards = [];
        $scope.stores = [];

        $scope.isDealsLoading = false;
        $scope.isStoresLoading = false;
        $scope.isSavingaCardsLoading = false;

        $scope.search = $stateParams.search;

        $scope.$on('saving-card-purchased', function (event, savingCard) {
            $scope.deals.forEach(function (deal) {
                if (!deal.can_redeem) {
                    if (deal.saving_cards.indexOf(savingCard.id) != -1) {
                        deal.can_redeem = true;
                    }
                }
            });
        });

        function retrieveDeals() {
            $scope.isDealsLoading = true;
            DealsService.searchDeals($scope.search, function (data) {
                var newDeals = [];
                data.data.forEach(function (deal) {
                    deal.desc = $sce.trustAsHtml(deal.desc);
                    newDeals.push(deal);
                });
                $scope.isDealsLoading = false;
                $scope.deals = $scope.deals.concat(newDeals);
            }, function (error) {
                $scope.isDealsLoading = false;
                toastr.error(error.message, 'Error');
            });
        }

        function retrieveStores() {
            $scope.isStoresLoading = true;
            StoresService.searchStores($scope.search, function (data) {
                $scope.isStoresLoading = false;
                $scope.stores = data.data;
            }, function (error) {
                $scope.isStoresLoading = false;
                toastr.error(error.message, 'Error');
            });
        }

        function retrieveSavingCards() {
            $scope.isSavingaCardsLoading = true;
            SavingCardsService.searchSavingCards($scope.search, function (data) {
                $scope.isSavingaCardsLoading = false;
                $scope.savingCards = data.data;
            }, function (error) {
                $scope.isSavingaCardsLoading = false;
                toastr.error(error.message, 'Error');
            });
        }

        function retrieveData() {
            retrieveStores();
            retrieveSavingCards();
            retrieveDeals();
        }

        retrieveData();
    }]);