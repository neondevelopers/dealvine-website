/**
 * Created by Shailesh-InfyOm on 12/6/2016.
 */
dealvineApp.controller('StoreDealsController', [
    '$scope', '$stateParams', 'StoresService', '$sce', 'MobileEventsService', 'Constants', 'toastr',
    function ($scope, $stateParams, StoresService, $sce, MobileEventsService, Constants, toastr) {
        $scope.deals = [];
        $scope.isLoading = true;

        function retrieveStoreDeals() {
            StoresService.retrieveStoreDeals($stateParams.store, function (data) {
                $scope.isLoading = false;
                var newDeals = [];
                data.data.deals.forEach(function (deal) {
                    deal.desc = $sce.trustAsHtml(deal.desc);
                    newDeals.push(deal);
                });
                $scope.deals = $scope.deals.concat(newDeals);
                data.deals = $scope.deals;
                $scope.store = data.data;
                sendStoreVisitEvent();
            }, function (error) {
                toastr.error(error.message, 'Error');
            })
        }

        function sendStoreVisitEvent() {
            var event = {
                'business_id': $scope.store.id,
                'application_id': Constants.Applications.Dealvine,
                'content_type_id': Constants.ContentTypes.Business,
                'content_id': $scope.store.id,
                'event_id': Constants.Event.VISIT,
                'event_data': '',
                'source': 'dealvine'
            };
            MobileEventsService.trackEvent(event, function (data) {

            }, function (error) {
                toastr.error(error.message, 'Error');
            });
        }

        function retrieveData() {
            retrieveStoreDeals();
        }

        retrieveData();
    }]);