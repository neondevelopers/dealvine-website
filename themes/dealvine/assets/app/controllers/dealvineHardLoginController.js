/**
 * Created by Mitul on 24-04-2017.
 */

dealvineApp.controller('DealvineHardLoginController', [
    '$scope', '$rootScope', 'HomeService', 'toastr', '$state',
    function ($scope, $rootScope, HomeService, toastr, $state) {
        $scope.isLogging = false;
        $scope.password = '';

        $scope.login = function() {
            $scope.isLogging = true;
            HomeService.doHardLogin($scope.password, function (data) {
                if (data.data.success) {
                    $scope.isLogging = false;
                    $state.go('home');
                }
            }, function (error) {
                $scope.isLogging = false;
                toastr.error("Invalid Password", 'Error');
            });
        }
    }]);