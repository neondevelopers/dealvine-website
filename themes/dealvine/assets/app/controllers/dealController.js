/**
 * Created by Mitul on 28/11/16.
 */
dealvineApp.controller('DealController', [
    '$scope', '$rootScope', 'DealsService', 'toastr', '$stateParams', 'SavingCardsService',
    function ($scope, $rootScope, DealsService, toastr, $stateParams, SavingCardsService) {
        $scope.deal = {};
        $scope.isLoading = true;

        function retrieveDealDetails() {
            DealsService.retrieveDealDetails($stateParams.id, function (data) {
                $scope.deal = data.data;
                $scope.isLoading = false;
            }, function (error) {
                toastr.error(error.message, 'Error');
            });
        }

        $scope.$on('saving-card-purchased', function (event, savingCard) {
            if (!$scope.deal.can_redeem) {
                if ($scope.deal.saving_cards.indexOf(savingCard.id) != -1) {
                    retrieveDealDetails();
                }
            }
        });

        $scope.redeemDeal = function (deal) {
            SavingCardsService.redeemDeal(deal);
        };

        retrieveDealDetails();
    }]);