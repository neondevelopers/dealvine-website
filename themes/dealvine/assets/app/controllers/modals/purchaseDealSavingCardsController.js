/**
 * Created by Mitul on 29/12/16.
 */
dealvineApp.controller('PurchaseDealSavingCardsController', [
    '$scope', '$rootScope', 'SavingCardsService', 'toastr', '$uibModalInstance', 'deal',
    function ($scope, $rootScope, SavingCardsService, toastr, $uibModalInstance, deal) {
        $scope.deal = deal;
        $scope.isLoading = true;
        $scope.savingCards = [];

        function retrieveSavingCards() {
            SavingCardsService.retrieveSavingCardsByIds($scope.deal.saving_cards, function (response) {
                $scope.isLoading = false;
                $scope.savingCards = response.data;
            }, function (error) {
                toastr.error(error.message, 'Error');
            });
        }

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        $scope.onPurchaseSuccess = function (savingCard) {
            $scope.deal.can_redeem = true;
            var url = baseUrl + 'api/d1/new/deals/' + $scope.deal.id + '/redeem';
            window.open(url, '_blank');
            $scope.cancel();
        };

        retrieveSavingCards();
    }]);