/**
 * Created by Mitul on 07/12/16.
 */
dealvineApp.controller('LoginController', [
    '$scope', '$rootScope', '$location', '$uibModalInstance',
    function ($scope, $rootScope, $location, $uibModalInstance) {

        var logInUrl = baseUrl + 'api/m2/login/';
        var redirectUrl = window.encodeURIComponent($location.absUrl());

        $scope.facebookUrl = logInUrl + 'Facebook?redirectUrl=' + redirectUrl;
        $scope.googleUrl = logInUrl + 'Google?redirectUrl=' + redirectUrl;
        $scope.twitterUrl = logInUrl + 'Twitter?redirectUrl=' + redirectUrl;

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        }
    }]);