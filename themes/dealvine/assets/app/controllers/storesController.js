/**
 * Created by Mitul on 28/11/16.
 */
dealvineApp.controller('StoresController', [
    '$scope', '$rootScope', 'StoresService', 'toastr',
    function ($scope, $rootScope, StoresService, toastr) {
        $scope.isLoading = true;
        $scope.featuredStores = [];
        $scope.storesLoaded = false;
        $scope.slickConfig = {
            lazyLoad: 'ondemand',
            infinite: true,
            slidesToShow: 6,
            adaptiveHeight: true,
            slidesToScroll: 1,
            nextArrow: '<div class="slider-next-arrow"></div>',
            prevArrow: '<div class="slider-pre-arrow"></div>'
        };
        $scope.dealsWithCategories = [];

        function retrieveFeaturedStores() {
            StoresService.retrieveFeaturedStores(function (data) {
                $scope.featuredStores = data.data;
                $scope.storesLoaded = true;
            }, function (error) {
                toastr.error(error.message, 'Error');
            });
        }

        function retrieveDealsWithCategories() {
            StoresService.retrieveDealsWithCategories(function (data) {
                $scope.isLoading = false;
                $scope.dealsWithCategories = [];
                data.data.forEach(function (category) {
                    category['slug'] = category.name.replace(/ /g, '-').toLocaleLowerCase();
                    $scope.dealsWithCategories.push(category);
                });
            }, function (error) {
                toastr.error(error.message, 'Error');
            });
        }

        function retrieveData() {
            retrieveFeaturedStores();
            retrieveDealsWithCategories();
        }

        retrieveData();
    }]);