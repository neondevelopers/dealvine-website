/**
 * Created by Mitul on 28/11/16.
 */
dealvineApp.controller('MySavingCardsController', [
    '$scope', '$rootScope', 'SavingCardsService', 'toastr',
    function ($scope, $rootScope, SavingCardsService, toastr) {
        $scope.savingCards = [];
        $scope.isLoading = true;
        $scope.noMoreSavingCards = false;

        $scope.loadMoreSavingCards = function () {
            retrieveMySavingCards();
        };

        function retrieveMySavingCards() {
            $scope.isLoading = true;
            SavingCardsService.retrieveMySavingCards(10, $scope.savingCards.length, function (data) {
                $scope.isLoading = false;
                if (data.data.length < 10) {
                    $scope.noMoreSavingCards = true;
                }
                $scope.savingCards = $scope.savingCards.concat(data.data);
            }, function (error) {
                toastr.error(error.message, 'Error');
            });
        }

        function retrieveData() {
            retrieveMySavingCards();
        }

        retrieveData();
    }]);