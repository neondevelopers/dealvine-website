/**
 * Created by Mitul on 28/11/16.
 */
dealvineApp.controller('DealsController', [
    '$scope', '$rootScope', 'DealsService', '$sce', 'toastr',
    function ($scope, $rootScope, DealsService, $sce, toastr) {
        $scope.deals = [];
        $scope.isLoading = true;
        $scope.noMoreDeals = false;

        $scope.loadMoreDeals = function () {
            retrieveDeals();
        };

        $scope.$on('saving-card-purchased', function (event, savingCard) {
            $scope.deals.forEach(function (deal) {
                if (!deal.can_redeem) {
                    if (deal.saving_cards.indexOf(savingCard.id) != -1) {
                        deal.can_redeem = true;
                    }
                }
            });
        });

        function retrieveDeals() {
            $scope.isLoading = true;
            DealsService.retrieveDeals(10, $scope.deals.length, function (data) {
                $scope.isLoading = false;
                if (data.data.length < 10) {
                    $scope.noMoreDeals = true;
                }
                var newDeals = [];
                data.data.forEach(function (deal) {
                    deal.desc = $sce.trustAsHtml(deal.desc);
                    newDeals.push(deal);
                });
                $scope.deals = $scope.deals.concat(newDeals);
            }, function (error) {
                toastr.error(error.message, 'Error');
            });
        }

        function retrieveData() {
            retrieveDeals();
        }

        retrieveData();
    }]);