/**
 * Created by Shailesh-InfyOm on 12/5/2016.
 */
dealvineApp.controller('CategoryDealsController', [
    '$scope', '$rootScope', '$stateParams', 'CategoryDealsService', 'toastr',
    function ($scope, $rootScope, $stateParams, CategoryDealsService, toastr) {
        $scope.category = [];
        $scope.isLoading = true;

        function retrieveCategoryDeals() {
            $scope.isLoading = true;
            CategoryDealsService.retrieveCategoryDeals($stateParams.category, function (data) {
                $scope.isLoading = false;
                $scope.category = data.data;
            }, function (error) {
                toastr.error(error.message, 'Error');
            });
        }

        function retrieveData() {
            retrieveCategoryDeals();
        }

        retrieveData();
    }]);