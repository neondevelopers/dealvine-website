/**
 * Created by Mitul on 28/11/16.
 */
dealvineApp.controller('SavingCardsController', [
    '$scope', '$rootScope', 'SavingCardsService', 'toastr',
    function ($scope, $rootScope, SavingCardsService, toastr) {
        $scope.categories = [];
        $scope.activeCategoryIndex = 0;
        $scope.activeCategory = null;
        $scope.storesLoaded = false;
        $scope.slickConfigs = {
            lazyLoad: 'ondemand',
            infinite: false,
            slidesToShow: 6,
            adaptiveHeight: true,
            slidesToScroll: 1
        };
        function retrieveSavingCards() {
            SavingCardsService.retrieveSavingCardsWithCategories(10, 0, function (data) {
                data.data.forEach(function (category) {
                    category.isSelected = false;
                    $scope.categories.push(category);
                });
                $scope.storesLoaded = true;
                $scope.activeCategory = $scope.categories[0];
                $scope.activeCategory.isSelected = true;
            }, function (error) {
                toastr.error(error.message, 'Error');
            });
        }

        $scope.selectCategory = function (index) {
            $scope.activeCategory.isSelected = false;
            $scope.activeCategory = $scope.categories[index];
            $scope.activeCategoryIndex = index;
            $scope.activeCategory.isSelected = true;
        };

        function retrieveData() {
            retrieveSavingCards();
        }

        retrieveData();
    }]);