/**
 * Created by Mitul on 07/12/16.
 */
dealvineApp.controller('UserController', [
    '$scope', '$rootScope', 'CustomerService', 'toastr',
    function ($scope, $rootScope, CustomerService, toastr) {
        $scope.birthDate = '';
        $scope.isUpdating = false;
        $scope.maxDate = new Date();
        $rootScope.$watch('customerProfile', function () {
            $scope.customerProfile = _.clone($rootScope.customerProfile);
            if ($scope.customerProfile != null)
                if ($scope.customerProfile.birthDay != '' && $scope.customerProfile.birthMonth != '' && $scope.customerProfile.birthYear != '') {
                    $scope.customerProfile.birthDate = new Date($scope.customerProfile.birthMonth + '/' + $scope.customerProfile.birthDay + '/' + $scope.customerProfile.birthYear);
                }
        }, true);
        $scope.customerProfile = _.clone($rootScope.customerProfile);
        $scope.updateProfile = function () {
            $scope.isUpdating = true;
            if ($scope.birthDate != '') {
                var output = convert($scope.birthDate);
                $scope.customerProfile.birthDay = output[2];
                $scope.customerProfile.birthMonth = output[1];
                $scope.customerProfile.birthYear = output[0];
            }
            CustomerService.updateCustomerProfile($scope.customerProfile, function (data) {
                $scope.isUpdating = false;
                swal(
                    'Success !!',
                    'Profile updated successfully',
                    'success'
                );
                $rootScope.customerProfile = data.data;
                $scope.customerProfile = $rootScope.customerProfile;
            }, function (error) {
                $scope.isUpdating = false;
                toastr.error(error.message, 'Error');
            });
        };
    }]);
function convert(str) {
    var date = new Date(str), month = ("0" + (date.getMonth() + 1)).slice(-2), day = ("0" + date.getDate()).slice(-2);
    return [date.getFullYear(), month, day];
}