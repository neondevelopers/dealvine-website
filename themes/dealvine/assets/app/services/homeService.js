/**
 * Created by Mitul on 28/11/16.
 */

/**
 * Created by Pavel on 11.06.2015.
 */
angular.module('dealvine.home-service', [])
    .factory('HomeService', ['$http', function ($http) {
        service.retrievePopularStores = function (successCallback, errorCallback) {
            $http({
                method: "GET",
                url: baseUrl + 'api/d1/new/popularStores'
            })
                .then(successCallback)
                .catch(errorCallback);
        };

        service.retrieveFlashSales = function (successCallback, errorCallback) {
            $http({
                method: "GET",
                url: baseUrl + 'api/d1/new/flashSales'
            })
                .then(successCallback)
                .catch(errorCallback);
        };

        service.retrieveCategories = function (successCallback, errorCallback) {
            $http({
                method: "GET",
                url: baseUrl + 'api/d1/new/categories'
            })
                .then(successCallback)
                .catch(errorCallback);
        };

        service.retrieveSpecialDeals = function (successCallback, errorCallback) {
            $http({
                method: "GET",
                url: baseUrl + 'api/d1/new/specialDeals'
            })
                .then(successCallback)
                .catch(errorCallback);
        };

        return service;
    }]);