/**
 * Created by Mitul on 08/12/16.
 */
angular.module('dealvine.mobile-events-service', [])
    .factory('MobileEventsService', ['$http', function ($http) {
        service.trackEvent = function (event, successCallback, errorCallback) {
            $http({
                method: "POST",
                url: baseUrl + 'api/m2/dealvine_event',
                data: event
            }).then(successCallback)
                .catch(errorCallback);
        };

        return service;
    }]);