/**
 * Created by Mitul on 28/11/16.
 */

/**
 * Created by Pavel on 11.06.2015.
 */
angular.module('dealvine.saving-card-service', [])
    .factory('SavingCardsService', ['$http', '$rootScope', 'StripeCheckout', '$uibModal',
        function ($http, $rootScope, StripeCheckout, $uibModal) {
            service.retrieveSavingCardsWithCategories = function (take, skip, successCallback, errorCallback) {
                $http({
                    method: "GET",
                    url: baseUrl + 'api/d1/new/savingCardsWithCategories'
                })
                    .then(successCallback)
                    .catch(errorCallback);
            };

            service.searchSavingCards = function (searchItem, successCallback, errorCallback) {
                return $http({
                    method: "POST",
                    url: baseUrl + 'api/d1/new/savingCards',
                    data: {
                        search: searchItem
                    }
                })
                    .then(successCallback)
                    .catch(errorCallback);
            };

            service.retrieveMySavingCards = function (take, skip, successCallback, errorCallback) {
                $http({
                    method: "GET",
                    url: baseUrl + 'api/d1/new/mySavingCards'
                })
                    .then(successCallback)
                    .catch(errorCallback);
            };

            service.purchaseSavingCard = function (savingCard, successCallback, errorCallback) {
                $http({
                    method: "POST",
                    url: baseUrl + 'api/m2/customers/' + savingCard.customer_id + '/saving-card/' + savingCard.card_id + '/purchase',
                    data: savingCard
                })
                    .then(successCallback)
                    .catch(errorCallback);
            };

            service.retrieveSavingCardDeals = function (savingCardId, successCallback, errorCallback) {
                $http({
                    method: "GET",
                    url: baseUrl + 'api/d1/new/savingCards/' + savingCardId + '/deals'
                })
                    .then(successCallback)
                    .catch(errorCallback);
            };

            service.retrieveSavingCardsByIds = function (savingCardIds, successCallback, errorCallback) {
                $http({
                    method: "POST",
                    url: baseUrl + 'api/d1/new/savingCards',
                    data: {
                        search: { id: savingCardIds }
                    }
                })
                    .then(successCallback)
                    .catch(errorCallback);
            };

            service.openLoginPopup = function () {
                $uibModal.open({
                    templateUrl: 'themes/dealvine/assets/app/templates/modals/login.html',
                    controller: 'LoginController',
                    size: 'md'
                });
            };

            service.redeemDeal = function (deal) {
                if ($rootScope.isLoggedIn) {
                    if (deal.can_redeem) {
                        var url = baseUrl + 'api/d1/new/deals/' + deal.id + '/redeem';
                        window.open(url, '_blank');
                    } else {
                        $uibModal.open({
                            templateUrl: 'themes/dealvine/assets/app/templates/modals/purchase-deal-saving-card.html',
                            controller: 'PurchaseDealSavingCardsController',
                            size: 'lg',
                            resolve: {
                                deal: function () {
                                    return deal;
                                }
                            }
                        });
                    }
                } else {
                    service.openLoginPopup();
                }
            };

            service.openPurchaseSavingCard = function (savingCard, customerId, successCallback, errorCallback) {
                var handler = StripeCheckout.configure({
                    name: "Purchase Saving Card",
                    token: function (token, args) {
                        console.log("Got stripe token: " + token.id);
                    }
                });

                var options = {
                    description: savingCard.name,
                    amount: savingCard.price * 100
                };

                handler.open(options)
                    .then(function (result) {
                        var token = result[0].id;
                        var purchaseObj = {
                            customer_id: customerId,
                            card_id: savingCard.id,
                            stripeToken: token,
                            amount: savingCard.price
                        };
                        service.purchaseSavingCard(purchaseObj, function (response) {
                            $rootScope.$broadcast('saving-card-purchased', savingCard);
                            savingCard.is_purchased = true;
                            swal(
                                'Success !!',
                                response.data.message,
                                'success'
                            );
                            successCallback(response.data);
                        }, function (error) {
                            var errorMessage = 'Failed to purchase saving card';
                            if (error && error.message) {
                                errorMessage = error.message;
                            }
                            swal(
                                'Failed !!',
                                errorMessage,
                                'error'
                            );
                            errorCallback(error);
                        });
                    }, function () {

                    });
            }

            return service;
        }]);