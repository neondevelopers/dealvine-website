/**
 * Created by Mitul on 06/12/16.
 */
angular.module('dealvine.customer-service', [])
    .factory('CustomerService', ['$http', '$q', '$rootScope', function ($http, $q, $rootScope) {
        service.isHardLoggedIn = function () {
            if ($rootScope.isHardLoggedInChecked) {
                if ($rootScope.hardLoggedIn) {
                    return true;
                } else {
                    return $q.reject('Not Authenticated');
                }
            } else {
                return $http({
                    method: "GET",
                    url: baseUrl + 'api/d1/is_hard_logged_in'
                }).then(function (data) {
                    $rootScope.isHardLoggedInChecked = true;
                    if (data.data.success) {
                        $rootScope.hardLoggedIn = true;
                        service.isCustomerLoggedIn();
                        return true;
                    } else {
                        return $q.reject('Not Authenticated');
                    }
                }).catch(function (error) {
                    $rootScope.isHardLoggedInChecked = true;
                    return $q.reject('Not Authenticated');
                });
            }
        };

        service.isAnonymous = function () {
            if ($rootScope.isHardLoggedInChecked) {
                if (!$rootScope.hardLoggedIn) {
                    return true;
                } else {
                    return $q.reject('Already Authenticated');
                }
            } else {
                return $http({
                    method: "GET",
                    url: baseUrl + 'api/d1/is_hard_logged_in'
                }).then(function (data) {
                    $rootScope.isHardLoggedInChecked = true;
                    if (!data.data.success) {
                        $rootScope.hardLoggedIn = false;
                        return true;
                    } else {
                        return $q.reject('Already Authenticated');
                    }
                }).catch(function (error) {
                    $rootScope.isHardLoggedInChecked = false;
                    return $q.reject('Already Authenticated');
                });
            }
        };

        service.doHardLogin = function (password, successCallback, errorCallback) {
            var data = {
                password: password
            };
            $http({
                method: "POST",
                url: baseUrl + 'api/d1/hard_login',
                data: data
            })
                .then(function (data) {
                    if (data.data.success) {
                        $rootScope.hardLoggedIn = true;
                    }
                    successCallback(data);
                })
                .catch(errorCallback);
        }

        service.isCustomerLoggedIn = function () {
            if ($rootScope.isLoggedInChecked) {
                if ($rootScope.isLoggedIn) {
                    return true;
                } else {
                    return $q.reject('Customer Not Authenticated');
                }
            } else {
                return $http({
                    method: "GET",
                    url: baseUrl + 'api/d1/is_logged_in'
                }).then(function (data) {
                    $rootScope.isLoggedInChecked = true;
                    if (data.data.success) {
                        $rootScope.isLoggedIn = true;
                        service.retrieveCustomerProfile();
                        return true;
                    } else {
                        return $q.reject('Customer Not Authenticated');
                    }
                }).catch(function (error) {
                    $rootScope.isLoggedInChecked = true;
                    return $q.reject('Customer Not Authenticated');
                });
            }
        };

        service.retrieveCustomerProfile = function (successCallback, errorCallback) {
            if ($rootScope.customerProfile) {
                return $rootScope.customerProfile;
            } else {
                $http({
                    method: "GET",
                    url: baseUrl + 'api/m2/customer_profile'
                }).then(function (data) {
                    $rootScope.customerProfile = data.data;
                    successCallback($rootScope.customerProfile);
                }).catch(errorCallback);
            }
        };

        service.updateCustomerProfile = function (customerProfile, successCallback, errorCallback) {
            $http({
                method: "POST",
                url: baseUrl + 'api/m2/customer_profile',
                data: customerProfile
            }).then(successCallback)
                .catch(errorCallback);
        };

        // service.isHardLoggedIn();

        return service;
    }]);