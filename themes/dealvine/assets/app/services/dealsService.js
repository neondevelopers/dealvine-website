/**
 * Created by Mitul on 28/11/16.
 */

/**
 * Created by Pavel on 11.06.2015.
 */
angular.module('dealvine.deals-service', [])
    .factory('DealsService', ['$http', function ($http) {
        service.retrieveDeals = function (take, skip, successCallback, errorCallback) {
            var url = baseUrl + 'api/d1/new/deals?';

            if (take) {
                url += "take=" + take;
            } else {
                url += "take=10";
            }

            if (skip) {
                url += "&skip=" + skip;
            }

            $http({
                method: "GET",
                url: url
            })
                .then(successCallback)
                .catch(errorCallback);
        };

        service.retrieveDealDetails = function (id, successCallback, errorCallback) {
            var url = baseUrl + 'api/d1/new/deals/' + id;

            $http({
                method: "GET",
                url: url
            })
                .then(successCallback)
                .catch(errorCallback);
        };

        service.search = function (searchItem) {
            return $http.get(baseUrl + 'api/d1/new/search', {
                params: {
                    search: searchItem
                }
            })
        };

        service.searchDeals = function (searchItem, successCallback, errorCallback) {
            return $http.get(baseUrl + 'api/d1/new/deals', {
                params: {
                    search: searchItem
                }
            })
                .then(successCallback)
                .catch(errorCallback);
        };

        service.retrieveHotDeals = function (take, skip, successCallback, errorCallback) {
            var url = baseUrl + 'api/d1/new/hotDeals?';

            if (take) {
                url += "take=" + take;
            } else {
                url += "take=10";
            }

            if (skip) {
                url += "&skip=" + skip;
            }

            $http({
                method: "GET",
                url: url
            })
                .then(successCallback)
                .catch(errorCallback);
        };

        return service;
    }]);