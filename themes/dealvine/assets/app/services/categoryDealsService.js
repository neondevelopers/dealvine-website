/**
 * Created by Shailesh-InfyOm on 12/5/2016.
 */
angular.module('dealvine.category-deals-service', [])
    .factory('CategoryDealsService', ['$http', function ($http) {
        service.retrieveCategoryDeals = function (category, successCallback, errorCallback) {
            $http({
                method: "GET",
                url: baseUrl + 'api/d1/new/categories/' + category + '/deals'
            }).then(successCallback)
                .catch(errorCallback);
        };

        return service;
    }]);