/**
 * Created by Mitul on 28/11/16.
 */
angular.module('dealvine.services', [
    'dealvine.home-service',
    'dealvine.deals-service',
    'dealvine.stores-service',
    'dealvine.saving-card-service',
    'dealvine.category-deals-service',
    'dealvine.customer-service',
    'dealvine.mobile-events-service'
]);