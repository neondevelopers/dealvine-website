/**
 * Created by Mitul on 28/11/16.
 */

/**
 * Created by Pavel on 11.06.2015.
 */
angular.module('dealvine.stores-service', [])
    .factory('StoresService', ['$http', function ($http) {
        service.retrieveFeaturedStores = function (successCallback, errorCallback) {
            $http({
                method: "GET",
                url: baseUrl + 'api/d1/new/featuredStores'
            })
                .then(successCallback)
                .catch(errorCallback);
        };

        service.retrieveDealsWithCategories = function (successCallback, errorCallback) {
            $http({
                method: "GET",
                url: baseUrl + 'api/d1/new/dealsWithCategories'
            })
                .then(successCallback)
                .catch(errorCallback);
        };

        service.retrieveStoreDeals = function (storeName, successCallback, errorCallback) {
            $http({
                method: "GET",
                url: baseUrl + 'api/d1/new/dealsForStore/' + storeName
            })
                .then(successCallback)
                .catch(errorCallback);
        };

        service.searchStores = function (searchItem, successCallback, errorCallback) {
            return $http.get(baseUrl + 'api/d1/new/stores', {
                params: {
                    search: searchItem
                }
            })
                .then(successCallback)
                .catch(errorCallback);
        };

        return service;
    }]);
