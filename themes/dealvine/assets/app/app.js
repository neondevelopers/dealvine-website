/**
 * Created by Mitul on 28/11/16.
 */
var dealvineApp = angular.module('dealvine', [
    'ui.router',
    'dealvine.directives',
    'dealvine.services',
    'slickCarousel',
    'ngSanitize',
    'ngAnimate',
    'stripe.checkout',
    'toastr',
    'ui.bootstrap',
    'angularLazyImg',
    'angular-spinkit'
]);

dealvineApp.constant('Constants', {
    Applications: {
        Dealvine: 7
    },
    ContentTypes: {
        Business: 3
    },
    Event: {
        CLICK: 1,
        VISIT: 2,
        BUY: 3,
        REDEEM: 4
    }
});

dealvineApp.config(function ($stateProvider,
                             $urlRouterProvider,
                             $httpProvider,
                             StripeCheckoutProvider,
                             lazyImgConfigProvider,
                             $qProvider) {

    $qProvider.errorOnUnhandledRejections(false);

    $urlRouterProvider.otherwise('/');
    $httpProvider.defaults.withCredentials = true;
    StripeCheckoutProvider.defaults({
        key: stripeKey
    });

    lazyImgConfigProvider.setOptions({
        offset: 25, // how early you want to load image (default = 100)
        errorClass: 'error', // in case of loading image failure what class should be added (default = null)
        successClass: 'success', // in case of loading image success what class should be added (default = null)
        onError: function (image) {
        }, // function fired on loading error
        onSuccess: function (image) {
        } // if scrollable container is not $window then provide it here
    });

    var hardLogin = {
        name: 'login',
        url: '/',
        controller: 'DealvineHardLoginController',
        templateUrl: 'themes/dealvine/assets/app/templates/hardLogin.html',
        resolve: {
            auth: function (CustomerService) {
                return CustomerService.isAnonymous();
            }
        }
    };

    var root = {
        name: 'root',
        abstract: true,
        template: '<div ui-view=""></div>',
        resolve: {
            stripe: StripeCheckoutProvider.load,
            auth: function (CustomerService) {
                return CustomerService.isHardLoggedIn();
            }
        }
    }

    var userLoggedIn = {
        name: 'userLoggedIn',
        abstract: true,
        template: '<div ui-view=""></div>',
        resolve: {
            stripe: StripeCheckoutProvider.load,
            auth: function (CustomerService) {
                return CustomerService.isHardLoggedIn();
            },
            customerAuth: function (CustomerService) {
                return CustomerService.isCustomerLoggedIn();
            }
        }
    }

    var home = {
        name: 'home',
        parent: 'root',
        url: '/home',
        controller: 'HomeController',
        templateUrl: 'themes/dealvine/assets/app/templates/home.html'
    };

    var deals = {
        name: 'deals',
        parent: 'root',
        url: '/deals',
        controller: 'DealsController',
        templateUrl: 'themes/dealvine/assets/app/templates/deals.html'
    };

    var stores = {
        name: 'stores',
        parent: 'root',
        url: '/stores',
        controller: 'StoresController',
        templateUrl: 'themes/dealvine/assets/app/templates/stores.html'
    };

    var savingCards = {
        name: 'savingCards',
        parent: 'root',
        url: '/saving-cards',
        controller: 'SavingCardsController',
        templateUrl: 'themes/dealvine/assets/app/templates/saving-cards.html'
    };

    var mySavingCards = {
        name: 'mySavingCards',
        parent: 'userLoggedIn',
        url: '/my-saving-cards',
        controller: 'MySavingCardsController',
        templateUrl: 'themes/dealvine/assets/app/templates/my-saving-cards.html'
    };

    var categoryDeals = {
        name: 'categoryDeals',
        parent: 'root',
        url: '/category-deals/:category',
        controller: 'CategoryDealsController',
        templateUrl: 'themes/dealvine/assets/app/templates/category-deals.html'
    };

    var storeDeals = {
        name: 'storeDeals',
        parent: 'root',
        url: '/store-deals/:store',
        controller: 'StoreDealsController',
        templateUrl: 'themes/dealvine/assets/app/templates/store-deals.html',
        params: {store: null}
    };

    var profile = {
        name: 'profile',
        parent: 'userLoggedIn',
        url: '/profile',
        controller: 'UserController',
        templateUrl: 'themes/dealvine/assets/app/templates/profile.html'
    };

    var hotDeals = {
        name: 'hotDeals',
        parent: 'root',
        url: '/hot-deals',
        controller: 'HotDealsController',
        templateUrl: 'themes/dealvine/assets/app/templates/hot-deals.html'
    };

    var savingCardDeals = {
        name: 'savingCardDeals',
        parent: 'root',
        url: '/saving-card/:id/deals',
        controller: 'SavingCardDealsController',
        templateUrl: 'themes/dealvine/assets/app/templates/saving-card-deals.html',
        params: {id: null}
    };

    var dealDetails = {
        name: 'dealDetails',
        parent: 'root',
        url: '/deal/:id',
        controller: 'DealController',
        templateUrl: 'themes/dealvine/assets/app/templates/deal-details.html',
        params: {id: null}
    };

    var search = {
        name: 'search',
        parent: 'root',
        url: '/search/:search',
        controller: 'SearchController',
        templateUrl: 'themes/dealvine/assets/app/templates/search.html',
        params: {search: null}
    };

    $stateProvider.state(hardLogin);
    $stateProvider.state(userLoggedIn);
    $stateProvider.state(root);
    $stateProvider.state(home);
    $stateProvider.state(deals);
    $stateProvider.state(stores);
    $stateProvider.state(savingCards);
    $stateProvider.state(mySavingCards);
    $stateProvider.state(categoryDeals);
    $stateProvider.state(storeDeals);
    $stateProvider.state(profile);
    $stateProvider.state(hotDeals);
    $stateProvider.state(savingCardDeals);
    $stateProvider.state(dealDetails);
    $stateProvider.state(search);
});

dealvineApp.run(function ($rootScope, $anchorScroll, $uibModalStack, $state, CustomerService) {
    $rootScope.isHomePage = false;

    $rootScope.isHardLoggedInChecked = false;
    $rootScope.hardLoggedIn = false;

    $rootScope.isLoggedInChecked = false;
    $rootScope.isLoggedIn = false;


    $rootScope.$on('$stateChangeSuccess',
        function (event, toState, toParams, fromState, fromParams) {
            if (toState.name == 'home') {
                $rootScope.isHomePage = true;
            } else {
                $rootScope.isHomePage = false;
                if (toState.name == "savingCardDeals") {
                    $uibModalStack.dismissAll();
                }
            }
        }
    );

    $rootScope.$on("$stateChangeError", function (event, toState, toParams, fromState, fromParams, error) {
        if (error == 'Not Authenticated') {
            $state.go('login');
        } else if (error == 'Already Authenticated' || error == 'Customer Not Authenticated') {
            $state.go('home');
        }
    })

    $rootScope.$on("$locationChangeSuccess", function () {
        $anchorScroll();
    });
});