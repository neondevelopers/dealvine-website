/**
 * Created by Mitul on 28/11/16.
 */

var service = angular.module('dealvine.directives', ['ui.bootstrap']);

service.directive('popularBrand', function () {
    return {
        restrict: 'E',
        templateUrl: 'themes/dealvine/assets/app/directives/brand.html',
        scope: {
            brand: '='
        }
    };
});

service.directive('flashSale', ['$rootScope', 'SavingCardsService', function ($rootScope, SavingCardsService) {
    return {
        restrict: 'E',
        templateUrl: 'themes/dealvine/assets/app/directives/flash_sale.html',
        scope: {
            flashSale: '='
        },
        link: function (scope) {
            scope.redeemDeal = function (deal) {
                SavingCardsService.redeemDeal(deal);
            }
        }
    }
}]);

service.directive('category', function () {
    return {
        restrict: 'E',
        templateUrl: 'themes/dealvine/assets/app/directives/category.html',
        scope: {
            category: '='
        }
    };
});

service.directive('deal', ['$rootScope', 'SavingCardsService', function ($rootScope, SavingCardsService) {
    return {
        restrict: 'E',
        templateUrl: 'themes/dealvine/assets/app/directives/deal.html',
        scope: {
            deal: '=',
            index: '@'
        },
        link: function (scope) {
            scope.redeemDeal = function (deal) {
                SavingCardsService.redeemDeal(deal);
            }
        }
    };
}]);

service.directive('specialDealHeader', function () {
    return {
        restrict: 'E',
        templateUrl: 'themes/dealvine/assets/app/directives/special_deal_header.html',
        scope: {
            specialDeal: '=',
            index: '@',
            selectedIndex: '='
        }
    };
});

service.directive('specialDeal', function () {
    return {
        restrict: 'E',
        templateUrl: 'themes/dealvine/assets/app/directives/special_deal.html',
        scope: {
            store: '='
        }
    };
});

service.directive('featuredStore', function () {
    return {
        restrict: 'E',
        templateUrl: 'themes/dealvine/assets/app/directives/featured_store.html',
        scope: {
            store: '='
        }
    };
});

service.directive('featuredCategory', function () {
    return {
        restrict: 'E',
        templateUrl: 'themes/dealvine/assets/app/directives/featured_category.html',
        scope: {
            category: '=',
            index: '@'
        }
    };
});

service.directive('storeCategoryBrand', ['$rootScope', 'SavingCardsService', function ($rootScope, SavingCardsService) {
    return {
        restrict: 'E',
        templateUrl: 'themes/dealvine/assets/app/directives/store_category_brand.html',
        scope: {
            deal: '='
        },
        link: function (scope) {
            scope.redeemDeal = function (deal) {
                SavingCardsService.redeemDeal(deal);
            }
        }
    };
}]);

service.directive('savingCard', ['SavingCardsService', '$rootScope',
    function (SavingCardsService, $rootScope) {
        return {
            restrict: 'E',
            templateUrl: 'themes/dealvine/assets/app/directives/saving_card.html',
            scope: {
                savingCard: '=',
                onPurchaseSuccess: '&'
            },
            link: function (scope) {
                scope.purchaseSavingCard = function (savingCard) {
                    if ($rootScope.isLoggedIn) {
                        SavingCardsService.openPurchaseSavingCard(savingCard, $rootScope.customerProfile.id,
                        function (data) {
                            scope.onPurchaseSuccess({savingCard: scope.savingCard});
                        }, function (error) {

                        });
                    } else {
                        SavingCardsService.openLoginPopup();
                    }
                }
            }
        };
    }]);
service.directive('mySavingCard', [
    function () {
        return {
            restrict: 'E',
            templateUrl: 'themes/dealvine/assets/app/directives/my_saving_card.html',
            scope: {
                savingCard: '='
            }
        };
    }]);